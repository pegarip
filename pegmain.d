/* Pegasus Engine
 * ripped by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * original code: http://www-ui.is.s.u-tokyo.ac.jp/~takeo/java/pegasus/pegasus-e.html
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module pegmain;

// uncomment to hold left button to draw
version = gst_left_draws;

//version = gst_use_pdollar;

import core.time;

import arsd.color;
import arsd.simpledisplay;

import iv.cmdcon;
import iv.sdpyutil;
import iv.strex;
import iv.unarray;
import iv.vfs;

import pegengine;

import pdollar0;
import pdollartest;
import gengpro2;


// ////////////////////////////////////////////////////////////////////////// //
version(gst_use_pdollar) {
  __gshared DPGestureList gsl;
  __gshared DPPoint[] hintPoints;
} else {
  __gshared PTGlyph[] gslist;
}
__gshared float[] hintPoints;
__gshared int hintPointsGIdx = -1;
__gshared string hintPointsName;


shared static this () {
  version(gst_use_pdollar) {
    gsl = new DPGestureList();
    try {
      gsl.load(VFile("zgest.gsl"));
    } catch (Exception e) {
      gsl.clear();
      gsl.addTestGestures();
      gsl.save(VFile("zgest.gsl", "w"));
    }
  } else {
    try {
      gslist = gstLibLoad(VFile("zgest.gsl"));
      auto g = gslist[0];
    } catch (Exception e) {
      import std.stdio;
      writeln("cannot load gestures: ", e.msg);
      gslist.length = 0;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Painter : PegPainter {
private:
  SimpleWindow win;
  Color clrfg = Color.black;

public:
  //alias drawWideLine = super.drawWideLine; // bring overloads

public:
  this (SimpleWindow aw) { window = aw; }

  @property void window (SimpleWindow aw) {
    if (win !is aw) {
      clrfg = Color.black;
      win = aw;
      colorFG = Color.white;
    }
  }

  override @property void colorFG (Color c) {
    if (c != clrfg) {
      clrfg = c;
      if (win is null || win.closed) return;
      uint clr = (cast(uint)c.r<<16)|(cast(uint)c.g<<8)|cast(uint)c.b;
      XSetForeground(win.impl.display, win.impl.gc, clr);
      XSetBackground(win.impl.display, win.impl.gc, clr);
    }
  }

  override void flush () {
    if (win is null || win.closed) return;
    XCopyArea(win.impl.display, cast(Drawable)win.impl.buffer, cast(Drawable)win.impl.window, win.impl.gc, 0, 0, win.width, win.height, 0, 0);
    flushGui();
  }

  override void cls () {
    if (win is null || win.closed) return;
    XFillRectangle(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, 0, 0, win.width, win.height);
  }

  override void fillRect (int x0, int y0, int wdt, int hgt) {
    if (win is null || win.closed) return;
    XFillRectangle(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x0, y0, wdt, hgt);
  }

  override void drawRect (int x0, int y0, int wdt, int hgt) {
    if (win is null || win.closed) return;
    XDrawRectangle(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x0, y0, wdt+1, hgt+1); //???
  }

  override void drawEllipse (int x0, int y0, int w, int h) {
    if (win is null || win.closed) return;
    if (w < 1 || h < 1) return;
    XSetLineAttributes(win.impl.display, win.impl.gc, 0/*width*/, 0/*LineSolid*/, 2/*CapRound*/, 1/*JoinRound*/);
    XDrawArc(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x0, y0, w, h, 0, 360*64);
  }

  override void drawLine (int x0, int y0, int x1, int y1) {
    if (win is null || win.closed) return;
    XSetLineAttributes(win.impl.display, win.impl.gc, 0/*width*/, 0/*LineSolid*/, 2/*CapRound*/, 1/*JoinRound*/);
    XDrawLine(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x0, y0, x1, y1);
  }

  override void drawWideLine (int x1, int y1, int x2, int y2, double w) {
    if (win is null || win.closed) return;
    int wdt = cast(int)w;
    if (wdt <= 1) { drawLine(x1, y1, x2, y2); return; }
    if (wdt > short.max/2) wdt = short.max/2;
    XSetLineAttributes(win.impl.display, win.impl.gc, wdt/*width*/, 0/*LineSolid*/, 2/*CapRound*/, 1/*JoinRound*/);
    XDrawLine(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x1, y1, x2, y2);
  }

  override void drawWideCircle (int x, int y, int radius, int w) {
    if (win is null || win.closed) return;
    if (w <= 1) w = 0;
    if (w > short.max/2) w = short.max/2;
    radius += w;
    if (radius < 0) return;
    if (radius == 1) { XDrawPoint(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x, y); return; }
    XSetLineAttributes(win.impl.display, win.impl.gc, w/*width*/, 0/*LineSolid*/, 2/*CapRound*/, 1/*JoinRound*/);
    XDrawArc(win.impl.display, cast(Drawable)win.impl.buffer, win.impl.gc, x-radius/2, y-radius/2, radius, radius, 0, 360*64);
  }

  override void copyToPixmap (ref XPixmap backimg) {
    if (win is null || win.closed) return;
    if (!backimg.hasObject || backimg.width != win.width || backimg.height != win.height) backimg = XPixmap(win);
    backimg.copyFromWinBuf(win);
  }

  override void blit (XPixmap backimg) {
    if (win is null || win.closed || !backimg.valid) return;
    backimg.blitAt(win, 0, 0);
  }

  mixin(import("hfont.d"));
}


// ////////////////////////////////////////////////////////////////////////// //
final class PegasusCanvas : InteractiveScene {
  enum WinTitle = "Pegasus rip-off";

  // ////////////////////////////////////////////////////////////////////// //
  char[] tiPrompt;
  char[] tiText; // text input
  void delegate (string s) tiDoneCB; // (s is null) for escape

  private void tiPutStr(bool clear=false) (ref char[] dest, const(char)[] s...) {
    import core.memory : GC;
    static if (clear) dest.unsafeArrayClear();
    foreach (char ch; s) dest.unsafeArrayAppend(ch);
  }

  void tiStart (const(char)[] prompt, const(char)[] initstr, void delegate (string s) donecb) {
    if (donecb is null) return;
    tiPutStr!true(tiPrompt, prompt);
    tiPutStr!true(tiText, initstr);
    tiDoneCB = donecb;
    operation = Operation.None;
    clearClickCounts();
    //gstClear();
    gstCurIndex = 0;
    refresh();
  }

  // ////////////////////////////////////////////////////////////////////// //
  MonoTime msgHideTime;
  char[] msgText;

  final class MessageCheckEvent {}
  MessageCheckEvent msgEvent;

  void showMessage (const(char)[] text, int toutmsecs=3500) {
    tiPutStr!true(msgText, text);
    if (msgText.length > 0) {
      msgHideTime = MonoTime.currTime+toutmsecs.msecs;
      if (msgEvent is null) msgEvent = new MessageCheckEvent();
      if (!win.eventQueued!MessageCheckEvent) win.postTimeout(msgEvent, 100);
    }
    update();
  }

  void drawMessage () {
    if (msgText.length == 0) return; // nothing more to do
    auto ctt = MonoTime.currTime;
    if (ctt >= msgHideTime) {
      tiPutStr!true(msgText, null);
      wpa.colorFG = Color(255, 255, 255);
      wpa.fillRect(0, win.height-20, win.width, 20);
      return;
    }
    wpa.colorFG = Color(0, 255, 255);
    wpa.fillRect(0, win.height-20, win.width, 20);
    wpa.colorFG = Color(0, 0, 0);
    (cast(Painter)wpa).drawText(5, win.height-10, msgText, 0.6);
    if (msgEvent is null) msgEvent = new MessageCheckEvent();
    if (!win.eventQueued!MessageCheckEvent) win.postTimeout(msgEvent, 100);
  }

  // ////////////////////////////////////////////////////////////////////// //
  DPPoint[] gstpoints;
  int gstCurIndex = 0; // 0: not recording

  void gstClear () {
    gstpoints.unsafeArrayClear();
  }

  void gstAddPoint (int x, int y) {
    if (gstCurIndex < 1) return;
    gstpoints.unsafeArrayAppend(DPPoint(x, y, gstCurIndex));
  }

  void gstRecognize () {
    if (gstpoints.length < 2) return;
    version(gst_use_pdollar) {
      auto res = gsl.recognize(gstpoints);
    } else {
      DPResult res;
      PTGlyph g2 = new PTGlyph;
      foreach (const ref dp; gstpoints[]) g2.appendPoint(cast(int)dp.x, cast(int)dp.y);
      g2.normalize();
      const PTGlyph gg = g2.findMatch(gslist, &res.score);
      g2.clear();
      delete g2;
      if (gg is null) {
        res = DPResult.init;
      } else {
        res.name = gg.name;
        conwriteln("gesture '", res.name, "' with score ", res.score);
        res.score = 1.0f;
      }
    }
    if (res.valid) {
      version(gst_use_pdollar) conwriteln("gesture '", res.name, "' with score ", res.score);
      if (res.score >= 0.45f) {
        switch (res.name) {
          case "quit":
            win.close();
            return;
          case "clear":
            clear();
            regen();
            return;
          case "save":
            conwriteln("saving scene to '", curfname, "'...");
            try {
              save(VFile(curfname, "w"));
              showMessage("saved: "~curfname);
            } catch (Exception e) {
              conwriteln("SAVE ERROR: ", e.msg);
            }
            return;
          case "load":
            conwriteln("loading scene from '", curfname, "'...");
            try {
              load(VFile(curfname));
              regen();
              showMessage("loaded: "~curfname);
            } catch (Exception e) {
              conwriteln("LOAD ERROR: ", e.msg);
              clear();
            }
            return;
          case "savenew":
            tiStart("Save name: ", curfname, delegate (string res) {
              if (res !is null && res.length) {
                import std.path : extension;
                if (!res.extension.strEquCI(".pgs")) res ~= ".pgs";
                curfname = res;
                win.title = WinTitle~": "~curfname;
                conwriteln("saving scene to '", curfname, "'...");
                try {
                  save(VFile(curfname, "w"));
                  showMessage("saved: "~curfname);
                } catch (Exception e) {
                  conwriteln("SAVE ERROR: ", e.msg);
                }
              }
            });
            return;
          case "loadnew":
            tiStart("Load name: ", curfname, delegate (string res) {
              if (res !is null && res.length) {
                import std.path : extension;
                if (!res.extension.strEquCI(".pgs")) res ~= ".pgs";
                curfname = res;
                win.title = WinTitle~": "~curfname;
                conwriteln("loading scene from '", curfname, "'...");
                try {
                  load(VFile(curfname));
                  regen();
                  showMessage("loaded: "~curfname);
                } catch (Exception e) {
                  conwriteln("LOAD ERROR: ", e.msg);
                  clear();
                }
              }
            });
            return;
          default:
        }
      }
    } else {
      conwriteln("gesture NOT recognized");
    }
  }

  void gstDraw () {
    if (gstCurIndex < 1) return;
    if (gstpoints.length > 4) {
      delete hintPoints;
      hintPoints = null;
      hintPointsGIdx = -1;
      hintPointsName = null;
    }
    wpa.colorFG = Color(255, 127, 0);
    foreach (immutable idx; 1..gstpoints.length) {
      if (gstpoints[idx-1].id == gstpoints[idx].id) {
        wpa.drawWideLineF(gstpoints[idx-1].x, gstpoints[idx-1].y, gstpoints[idx].x, gstpoints[idx].y, 3);
      }
    }
  }

  void gstHintDraw () {
    if (hintPoints.length < 4) return;
    float[4] bbox = void;
    PTGlyph.calcBBox(bbox[], hintPoints);
    immutable float wdt = bbox[2]-bbox[0];
    immutable float hgt = bbox[3]-bbox[1];
    immutable float maxsz = (wdt > hgt ? wdt : hgt);
    if (maxsz > 0.0f) {
      const float ofsx = 100.0f;
      const float ofsy = 100.0f;
      const float scale = 320.0f/maxsz;
      int px = cast(int)((hintPoints[0]-bbox[0])*scale+ofsx);
      int py = cast(int)((hintPoints[1]-bbox[1])*scale+ofsy);
      foreach (immutable idx; 1..hintPoints.length/2U) {
        immutable int x = cast(int)((hintPoints[idx*2U+0]-bbox[0])*scale+ofsx);
        immutable int y = cast(int)((hintPoints[idx*2U+1]-bbox[1])*scale+ofsy);
        immutable int ct = cast(int)((cast(float)idx/cast(float)cast(int)(hintPoints.length/2U-1U))*255.0f);
        wpa.colorFG = Color(0, ct, 255-ct);
        wpa.drawWideLineF(px, py, x, y, 3);
        px = x;
        py = y;
      }
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  SimpleWindow win;
  string curfname = "zscene.pgs";

  // ////////////////////////////////////////////////////////////////////// //
  this (SimpleWindow aw) {
    win = aw;
    win.handleMouseEvent = delegate (MouseEvent e) {
      if (tiDoneCB !is null) return;
      if (e.type == MouseEventType.motion && (e.modifierState&(ModifierState.leftButtonDown|ModifierState.middleButtonDown|ModifierState.rightButtonDown)) != 0) {
        if (gstCurIndex) {
          version(gst_left_draws) {
            if ((e.modifierState&ModifierState.leftButtonDown) != 0) {
              gstAddPoint(e.x, e.y);
              refresh();
            }
          } else {
            if ((e.modifierState&ModifierState.leftButtonDown) == 0) {
              gstAddPoint(e.x, e.y);
              refresh();
            }
          }
          clearClickCounts();
          return;
        }
      }
      // right button down, left button draws segment
      if (e.type == MouseEventType.buttonPressed) {
        if ((e.modifierState&ModifierState.rightButtonDown) != 0) {
          if (e.button == MouseButton.left) {
            if (gstCurIndex == 0) gstClear();
            ++gstCurIndex;
            operation = Operation.None;
            refresh();
          }
        }
        if (gstCurIndex) {
          // oops, gesturing
          operation = Operation.None;
          clearClickCounts();
          return;
        }
      }
      // mouse release
      if (e.type == MouseEventType.buttonReleased) {
        if (gstCurIndex) {
          operation = Operation.None; // just in case
          if (e.button == MouseButton.right) {
            // gesture ends
            gstCurIndex = 0;
            if (e.modifierState&ModifierState.ctrl) {
              // ctrl held: save gesture
              if (gstpoints.length > 1) {
                immutable bool orient = !(e.modifierState&ModifierState.alt);
                tiStart((orient ? "Gesture name: " : "Gesture name(A): "), null, delegate (string res) {
                  gstCurIndex = 0;
                  if (res !is null && res.length) {
                    conwriteln("adding gesture '", res, "'");
                    version(gst_use_pdollar) {
                      try {
                        gsl.appendGesture(res, gstpoints);
                        gsl.save(VFile("zgest.gsl", "w"));
                      } catch (Exception e) {
                        conwriteln("ERROR: ", e.msg);
                      }
                    } else {
                      PTGlyph g2 = new PTGlyph;
                      g2.name = res;
                      g2.oriented = orient;
                      foreach (const ref dp; gstpoints[]) g2.appendPoint(cast(int)dp.x, cast(int)dp.y);
                      g2.normalize(false);
                      gslist ~= g2;
                      try {
                        gstLibSave(VFile("zgest.gsl", "w"), gslist);
                      } catch (Exception e) {
                        conwriteln("ERROR: ", e.msg);
                      }
                    }
                  }
                });
                gstCurIndex = 1; // so gesture will be shown
              }
            } else {
              gstRecognize();
            }
            refresh();
          }
          clearClickCounts();
          return;
        }
      }
      onMouseEvent(e);
    };
    win.handleCharEvent = delegate (dchar ch) {
      if (tiDoneCB is null) return;
      //conwriteln(cast(uint)ch);
      if (ch == 8) {
        if (tiText.length) {
          tiText.unsafeArrayRemove(cast(int)tiText.length-1);
          refresh();
          return;
        }
      }
      if (ch == 13 || ch == 10) {
        scope(success) refresh();
        scope(exit) tiDoneCB = null;
        tiDoneCB(tiText.length ? tiText.idup : "");
      }
      if (ch == 27) {
        scope(success) refresh();
        scope(exit) tiDoneCB = null;
        tiDoneCB(null);
      }
      // C-Y
      if (ch == 25) {
        if (tiText.length) {
          tiText.unsafeArrayClear();
          refresh();
        }
        return;
      }
      if (ch >= ' ' && ch < 127) {
        tiPutStr(tiText, cast(char)ch);
        refresh();
        return;
      }
    };
    win.addEventListener((MessageCheckEvent evt) {
      if (msgText.length == 0) return; // nothing to do
      auto ctt = MonoTime.currTime;
      if (ctt >= msgHideTime) {
        tiPutStr!true(msgText, null);
        wpa.colorFG = Color(255, 255, 255);
        wpa.fillRect(0, win.height-20, win.width, 20);
        update();
        return;
      }
      if (msgEvent is null) msgEvent = new MessageCheckEvent();
      if (!win.eventQueued!MessageCheckEvent) win.postTimeout(msgEvent, 100);
      update();
    });
    win.title = WinTitle~": "~curfname;
    super(new Painter(aw));
  }

  override void clear () {
    gstClear();
    gstCurIndex = 0;
    super.clear();
  }

  override void paint () {
    super.paint();
    gstDraw();
    gstHintDraw();
    drawMessage();
    if (tiDoneCB !is null) {
      wpa.colorFG = Color(0, 0, 80);
      wpa.fillRect(0, 0, win.width, 40);
      wpa.colorFG = Color(255, 127, 0);
      int tw = (cast(Painter)wpa).drawText(0, 20, tiPrompt);
      tw += (cast(Painter)wpa).drawText(tw, 20, tiText);
      wpa.colorFG = Color.yellow;
      wpa.drawLine(tw, 0, tw, 40);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
void main () {
  sdpyWindowClass = "PEGASUS_RIPOFF";
  auto sdwin = new SimpleWindow(1280, 1024, "Pegasus rip-off");
  auto cv = new PegasusCanvas(sdwin);
  cv.regen();
  cv.showMessage("Pegasus started...");
  bool inCX = false;
  sdwin.handleKeyEvent = delegate (KeyEvent e) {
    if (!e.pressed) return;
    if (e == "C-X" || e == "M-X") {
      if (inCX) { inCX = false; return; }
      inCX = true;
      return;
    }
    if (inCX) {
      inCX = false;
      if (e == "C-R" || e == "R" || e == "Delete") {
        cv.tiStart("Gst to remove: ", null, delegate (string res) {
          if (res !is null) {
            conwriteln("removing gesture '", res, "'");
            version(gst_use_pdollar) {
              try {
                gsl.removeGesture(res);
                gsl.save(VFile("zgest.gsl", "w"));
              } catch (Exception e) {
                conwriteln("ERROR: ", e.msg);
                //gsl.clear();
              }
            } else {
              foreach (immutable idx, PTGlyph g; gslist) {
                if (g.name == res) {
                  delete gslist[idx];
                  foreach (immutable c; idx+1..gslist.length) gslist[c-1] = gslist[c];
                  gslist[$-1] = null;
                  gslist.length -= 1;
                  try {
                    gstLibSave(VFile("zgest.gsl", "w"), gslist);
                  } catch (Exception e) {
                    conwriteln("ERROR: ", e.msg);
                  }
                }
              }
            }
          }
        });
        return;
      }
      if (e == "C-S" || e == "S" || e == "F2") {
        conwriteln("saving gestures...");
        version(gst_use_pdollar) {
          gsl.save(VFile("zgest.gsl", "w"));
        } else {
          gstLibSave(VFile("zgest.gsl", "w"), gslist);
        }
        return;
      }
      if (e == "C-L" || e == "L" || e == "F3") {
        conwriteln("reloading gestures...");
        version(gst_use_pdollar) {
          try {
            gsl.load(VFile("zgest.gsl"));
          } catch (Exception e) {
            conwriteln("ERROR: ", e.msg);
            gsl.clear();
          }
        } else {
          try {
            gslist = gstLibLoad(VFile("zgest.gsl"));
          } catch (Exception e) {
            conwriteln("ERROR: ", e.msg);
            gslist.length = 0;
          }
        }
        return;
      }
      if (e == "C-V" || e == "V" || e == "F1") {
        conwriteln("known gestures...");
        version(gst_use_pdollar) {
          foreach (string name; gsl.knownGestureNames) conwriteln("  [", name, "]");
        } else {
          foreach (immutable idx, PTGlyph g; gslist) {
            bool nseen = false;
            foreach (immutable i2; 0..idx) {
              if (gslist[i2].name == gslist[idx].name) {
                nseen = true;
                break;
              }
            }
            if (!nseen) conwriteln("  [", gslist[idx].name, "]");
          }
        }
        cv.tiStart("Gst to show: ", "", delegate (string res) {
          if (res !is null) {
            version(gst_use_pdollar) {
              delete hintPoints;
              hintPoints = null;
              hintPointsGIdx = -1;
              hintPointsName = null;
              auto cld = gsl.findGesture(res);
              if (cld) {
                hintPoints.length = cld.NumPoints*2;
                foreach (immutable idx; 0..cld.NumPoints) {
                  hintPoints[idx*2U+0U] = cld.points[idx].x;
                  hintPoints[idx*2U+1U] = cld.points[idx].y;
                }
              }
            } else {
              delete hintPoints;
              hintPoints = null;
              //hintPointsGIdx = -1;
              //hintPointsName = null;
                   if (res.length == 0) res = hintPointsName;
              else if (res != hintPointsName) hintPointsGIdx = -1;
              hintPointsName = res;
              foreach (immutable idx, PTGlyph g; gslist) {
                if (hintPointsGIdx >= 0 && cast(int)idx <= hintPointsGIdx) continue;
                if (g.name == res) {
                  //conwriteln("FOUND!");
                  hintPointsGIdx = cast(int)idx;
                  hintPoints.length = g.patPointCount*2;
                  //conwriteln("len=", hintPoints.length);
                  foreach (immutable pidx; 0..hintPoints.length/2U) {
                    immutable auto pt = g.patPoint(pidx);
                    hintPoints[pidx*2U+0] = pt.x;
                    hintPoints[pidx*2U+1] = pt.y;
                  }
                  break;
                }
              }
              //if (hintPoints is null) hintPointsGIdx = -1;
            }
          } else {
            delete hintPoints;
            hintPoints = null;
            hintPointsGIdx = -1;
          }
        });
        return;
      }
      // regen intervals
      if (e == "C-0" ) {
        cv.regenIntervals();
        return;
      }
    }
    if (e == "C-Q") { inCX = false; sdwin.close(); return; }
    //if (e == "F2") { inCX = false; cv.save(VFile("zscene.pgs", "w")); return; }
    //if (e == "F3") { inCX = false; cv.load(VFile("zscene.pgs")); return; }
    if (e == "C-F11") { inCX = false; cv.clear(); cv.regen(); return; }
    //conwriteln(e.toStr);
  };
  sdwin.eventLoop(0);
}
