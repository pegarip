/* Pegasus Engine
 * ripped by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * original code: http://www-ui.is.s.u-tokyo.ac.jp/~takeo/java/pegasus/pegasus-e.html
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
module pegengine;
private:

version = pegasus_interval_checks;

import arsd.color;
import arsd.simpledisplay;

import iv.alice;
import iv.cmdcon;
import iv.sdpyutil;
import iv.unarray;
import iv.vfs;


// ////////////////////////////////////////////////////////////////////////// //
enum ErrorThreshold = 0.00001;
bool fpequal() (double a, double b) { pragma(inline, true); import std.math : abs; return (abs(a-b) < ErrorThreshold); }
bool neglible() (double a) { pragma(inline, true); import std.math : abs; return (abs(a) < ErrorThreshold); }
int sign() (double t) { pragma(inline, true); return (t >= 0.0 ? 1 : -1); }


// ////////////////////////////////////////////////////////////////////////// //
public struct Vector2 {
nothrow @safe @nogc:
  static double distance (double x1, double y1, double x2, double y2) {
    pragma(inline, true);
    import std.math : sqrt;
    return sqrt((x2-x1)*(x2-x1)+(y2-y1)*(y2-y1));
  }

public:
  double x = 0;
  double y = 0;

public:
  this (double ax, double ay) {
    pragma(inline, true);
    x = ax;
    y = ay;
  }

  this (Node start, Node end) {
    pragma(inline, true);
    x = end.x-start.x;
    y = end.y-start.y;
  }

  double length () const { pragma(inline, true); import std.math : sqrt; return sqrt(x*x+y*y); }

  Vector2 normalize () const {
    pragma(inline, true);
    double len = length;
    if (len == 0.0) len = 1.0; else len = 1.0/length;
    return Vector2(x*len, y*len);
  }

  double innerProduct (Vector2 v) const { pragma(inline, true); return x*v.x+y*v.y; }
  double outerProduct (Vector2 v) const { pragma(inline, true); return x*v.y-y*v.x; }

  Vector2 rotate (double degree) const {
    import std.math : cos, sin, PI;
    if (degree == 90) return Vector2(-y, x);
    if (degree == 180) return Vector2(-x, -y);
    if (degree == 270) return Vector2(y, -x);
    double radian = (degree*PI)/180.0;
    double cosv = cos(radian);
    double sinv = sin(radian);
    return Vector2(x*cosv-y*sinv, x*sinv+y*cosv);
  }

  double getCos (Vector2 v) const {
    pragma(inline, true);
    import std.math : sqrt;
    double length = sqrt((x*x+y*y)*(v.x*v.x+v.y*v.y));
    return (length > 0.0 ? innerProduct(v)/length : 0.0);
  }

  double getSin (Vector2 v) const {
    pragma(inline, true);
    import std.math : sqrt;
    double length = sqrt((x*x+y*y)*(v.x*v.x+v.y*v.y));
    return (length > 0.0 ? outerProduct(v)/length : 0.0);
  }

  Vector2 scale (double scale) const { pragma(inline, true); return Vector2(x*scale, y*scale); }
}


// ////////////////////////////////////////////////////////////////////////// //
///
public abstract class PegPainter {
private:
  enum isGoodCT(T) =
    (__traits(isFloating, T) &&
     !is(T == ifloat) &&
     !is(T == idouble) &&
     !is(T == ireal) &&
     !is(T == cfloat) &&
     !is(T == cdouble) &&
     !is(T == creal)) ||
    (__traits(isIntegral, T) &&
     !is(T == bool) &&
     !is(T == char) &&
     !is(T == wchar) &&
     !is(T == dchar));

public:
  enum MarkSize = 10;

public:
  /// set drawing color
  @property void colorFG (Color c);

  /// flush backbuffer
  void flush ();

  // clear backbuffer to FG color
  void cls ();

  /// draw fill rectange
  void fillRect (int x0, int y0, int wdt, int hgt);

  /// draw rectangle outline
  void drawRect (int x0, int y0, int wdt, int hgt);

  /// draw ellipse outline
  void drawEllipse (int x0, int y0, int w, int h);

  /// draw line
  void drawLine (int x0, int y0, int x1, int y1);

  /// draw wide line (`w` pixels wide)
  void drawWideLine (int x1, int y1, int x2, int y2, double w);

  /// draw wide circle outline (`w` pixels wide)
  void drawWideCircle (int x, int y, int radius, int w);

  /// should create/resize pixmap if necessary
  void copyToPixmap (ref XPixmap backimg);

  // blit pixmap
  void blit (XPixmap backimg);

  final void drawWideLineF(T0, T1, T2, T3, T4) (T0 x1, T1 y1, T2 x2, T3 y2, T4 w)
  if (isGoodCT!T0 && isGoodCT!T1 && isGoodCT!T2 && isGoodCT!T3 && isGoodCT!T4)
  {
    drawWideLine(cast(int)x1, cast(int)y1, cast(int)x2, cast(int)y2, cast(double)w);
  }

  final void drawSlopeMark (int x1, int y1, int x2, int y2) {
    Vector2 normalVector = (Vector2(x2-x1, y2-y1)).normalize();
    Vector2 h1 = normalVector.rotate(150).scale(20);
    Vector2 h2 = normalVector.rotate(210).scale(20);
    immutable double mx = (x1+2*x2)/3.0;
    immutable double my = (y1+2*y2)/3.0;
    drawWideLineF(mx, my, mx+h1.x, my+h1.y, 3);
    drawWideLineF(mx, my, mx+h2.x, my+h2.y, 3);
  }

  final void drawParallelMark (int x1, int y1, int x2, int y2) {
    Vector2 normalVector = (Vector2(x2-x1, y2-y1)).normalize();
    Vector2 h1 = normalVector.rotate(150).scale(MarkSize);
    Vector2 h2 = normalVector.rotate(210).scale(MarkSize);
    drawWideLineF(x1, y1, x1-h1.x, y1-h1.y, 3);
    drawWideLineF(x1, y1, x1-h2.x, y1-h2.y, 3);
    drawWideLineF(x2, y2, x2+h1.x, y2+h1.y, 3);
    drawWideLineF(x2, y2, x2+h2.x, y2+h2.y, 3);
    drawWideLine(x1, y1, x2, y2, 3);
  }

  final void drawSlopeMark2 (int x1, int y1, int x2, int y2) {
    Vector2 n = (Vector2(x2-x1, y2-y1)).normalize().scale(MarkSize);
    Vector2 h1 = n.rotate(90);
    Vector2 h2 = h1.scale(2);
    immutable double mx = (x1+2*x2)/3.0;
    immutable double my = (y1+2*y2)/3.0;
    drawWideLineF(mx, my, mx+h1.x, my+h1.y, 3);
    drawWideLineF(mx+h1.x, my+h1.y, mx+h1.x+n.x, my+h1.y+n.y, 3);
    drawWideLineF(mx+n.x, my+n.y, mx+n.x+h2.x, my+n.y+h2.y, 3);
  }

  final void drawVerticalMark (int x, int y) {
    drawWideLine(x, y-MarkSize, x, y+MarkSize, 3);
  }

  final void drawAngleMark (int x1, int y1, int x2, int y2) {
    if (x1 == x2 || y1 == y2) return;
    Vector2 n = (Vector2(x2-x1, y2-y1)).normalize().scale(MarkSize);
    if (n.y > 0.0) n = n.rotate(180);
    Vector2 b = Vector2(MarkSize, 0.0);
    Vector2 n2 = n.scale(2);
    Vector2 b2 = b.scale(2);
    immutable double mx = (x1+2*x2)/3.0;
    immutable double my = (y1+2*y2)/3.0;
    drawWideLineF(mx-b2.x, my-b2.y, mx+b2.x, my+b2.y, 3);
    drawWideLineF(mx, my, mx+n2.x, my+n2.y, 3);
    drawWideLineF(mx+b.x, my+b.y, mx+n.x, my+n.y, 3);
  }

  final void drawHorizontalMark (int x, int y) {
    drawWideLine(x-MarkSize, y, x+MarkSize, y, 3);
  }

  final void drawCongruentMark (int x1, int y1, int x2, int y2) {
    Vector2 n = (Vector2(x2-x1, y2-y1)).normalize().scale(5);
    Vector2 h = n.rotate(90).scale(2);
    immutable double mx = (x1+x2)/2.0;
    immutable double my = (y1+y2)/2.0;
    drawWideLineF(mx+h.x, my+h.y, mx-h.x, my-h.y, 3);
    drawWideLineF(mx+n.x+h.x, my+n.y+h.y, (mx+n.x)-h.x, (my+n.y)-h.y, 3);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public struct Node {
public:
  double x, y;

public:
  string toString () const {
    import std.format;
    return "(%s,%s)".format(x, y);
  }

nothrow @safe @nogc:
  this (double ax, double ay) {
    x = ax;
    y = ay;
  }

final:
  bool valid () const { pragma(inline, true); import std.math : isNaN; return (!x.isNaN && !y.isNaN); }

  bool same() (in auto ref Node node) const { pragma(inline, true); return fpequal(node.x, x) && fpequal(node.y, y); }

  double distance() (in auto ref Node node) const {
    pragma(inline, true);
    import std.math : sqrt;
    return sqrt((node.x-x)*(node.x-x)+(node.y-y)*(node.y-y));
  }

static:
  Node invalid () { pragma(inline, true); return Node(double.nan, double.nan); }

  double distance() (in auto ref Node n1, in auto ref Node n2) {
    pragma(inline, true);
    import std.math : sqrt;
    return sqrt((n1.x-n2.x)*(n1.x-n2.x)+(n1.y-n2.y)*(n1.y-n2.y));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
struct CloseEnough {
private:
  enum MINIMUM_DISTINGUISHABLE = 20.0;
  double minimumDistinguishable;

public:
  bool[4] flag;
  double[4] originalCoords;

public nothrow @nogc:
  this (Segment stroke, double size) {
    minimumDistinguishable = MINIMUM_DISTINGUISHABLE/size;
    flag[] = false;
    foreach (immutable int i, ref double v; originalCoords[]) v = stroke.coords(i);
  }

  bool notFound (int i) { pragma(inline, true); return !flag[i]; }

  void check (Element element) {
    import std.math : abs;
    foreach (immutable int i; 0..4) {
      if (element.fixed(i) && abs(element.variables[i]-originalCoords[i]) < minimumDistinguishable) flag[i] = true;
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class Segment {
private:
  static shared ulong lastSegId = 0;

  static ulong getNextSegId () nothrow @trusted @nogc {
    import core.atomic;
    for (;;) {
      ulong res = atomicOp!"+="(lastSegId, 1);
      if (res) return res;
    }
  }

private:
  ulong mId;

public:
  double x1, y1, x2, y2;

public:
  override string toString () const {
    import std.format : format;
    if (!valid) return "<invalid>";
    return "(%s,%s)-(%s,%s)".format(x1, y1, x2, y2);
  }

protected:
  void save (VFile fl) const {
    fl.writeNum!double(x1);
    fl.writeNum!double(y1);
    fl.writeNum!double(x2);
    fl.writeNum!double(y2);
  }

  static Segment loadNew (ubyte ver, VFile fl) {
    ulong oid = (ver == 0 ? fl.readNum!ulong : 0); // only version 0 keeps this
    double x1 = fl.readNum!double;
    double y1 = fl.readNum!double;
    double x2 = fl.readNum!double;
    double y2 = fl.readNum!double;
    auto res = new Segment(x1, y1, x2, y2);
    //res.mOldId = oid;
    return res;
  }

public:
  this () { mId = getNextSegId(); }

  this (double ax1, double ay1, double ax2, double ay2) nothrow @trusted @nogc {
    mId = getNextSegId();
    x1 = ax1;
    y1 = ay1;
    x2 = ax2;
    y2 = ay2;
    //conwriteln("  0:new seg: (", this.x1, ",", this.y1, ")-(", this.x2, ",", this.y2, ") (", this.id, "; len=", this.length, ")");
    //if (this.length < ErrorThreshold) assert(0, "zero-length segment");
  }

  this() (in auto ref Node start, in auto ref Node end) nothrow @trusted @nogc {
    assert(start.valid);
    assert(end.valid);
    mId = getNextSegId();
    x1 = start.x;
    y1 = start.y;
    x2 = end.x;
    y2 = end.y;
    //conwriteln("  1:new seg: (", this.x1, ",", this.y1, ")-(", this.x2, ",", this.y2, ") (", this.id, "; len=", this.length, ")");
    //if (this.length < ErrorThreshold) assert(0, "zero-length segment");
  }

  final ulong id () const pure nothrow @safe @nogc { pragma(inline, true); return mId; }

  final bool valid () const nothrow @safe @nogc { pragma(inline, true); import std.math : isNaN; return (!x1.isNaN && !y1.isNaN && !x2.isNaN && !y2.isNaN); }

  final Vector2 vector () const nothrow @safe @nogc { pragma(inline, true); return Vector2(x2-x1, y2-y1); }

  final Node startNode () const nothrow @safe @nogc { pragma(inline, true); return Node(x1, y1); }
  final Node endNode () const nothrow @safe @nogc { pragma(inline, true); return Node(x2, y2); }

  final double length () const nothrow @safe @nogc { pragma(inline, true); return Vector2.distance(x1, y1, x2, y2); }

  final bool same (Segment s) const nothrow @safe @nogc {
    return startNode.same(s.startNode) && endNode.same(s.endNode) || startNode.same(s.endNode) && endNode.same(s.startNode);
  }

  final bool online() (in auto ref Node node) const nothrow @safe @nogc {
    pragma(inline, true);
    return (distanceLine(node) < ErrorThreshold);
  }

  final bool between() (in auto ref Node node) const nothrow @safe @nogc {
    pragma(inline, true);
    import std.algorithm : max, min;
    if (fpequal(y1, y2)) {
      return (node.x >= min(x1, x2)-ErrorThreshold && node.x <= max(x1, x2)+ErrorThreshold);
    } else {
      return (node.y >= min(y1, y2)-ErrorThreshold && node.y <= max(y1, y2)+ErrorThreshold);
    }
  }

  final bool parallel (Segment segment) const nothrow @safe @nogc {
    import std.math : abs;
    Vector2 vector1 = vector();
    Vector2 vector2 = segment.vector();
    return (vector1.getSin(vector2) < ErrorThreshold);
  }

  final double parallelInterval (Segment segment) const nothrow @safe {
    Vector2 normalVectorX = vector().normalize();
    Segment segment1 = coordSystem(normalVectorX);
    Segment segment2 = segment.coordSystem(normalVectorX);
    if ((segment1.x1-segment2.x2)*(segment1.x2-segment2.x1) < 0.0 ||
        (segment1.x1-segment2.x2)*(segment1.x2-segment2.x2) < 0.0 ||
        (segment2.x1-segment1.x1)*(segment2.x2-segment1.x1) < 0.0)
    {
      import std.math : abs;
      double interval = segment2.y1-segment1.y1;
      return abs(interval);
    }
    return -1.0;
  }

  final Node crossNode (Segment s) const nothrow @trusted @nogc {
    import std.math : abs;
    double a0 = y1-y2;
    double b0 = x2-x1;
    double c0 = y2*x1-x2*y1;
    double a1 = s.y1-s.y2;
    double b1 = s.x2-s.x1;
    double c1 = s.y2*s.x1-s.x2*s.y1;
    if (abs(a0*b1-a1*b0) < ErrorThreshold) {
      //{ import core.stdc.stdio : stderr, fprintf; stderr.fprintf("ERROR: error in Segment.crossNode()\n"); }
      return Node.invalid;
    } else {
      double x = (b0*c1-b1*c0)/(a0*b1-a1*b0);
      double y = (a0*c1-a1*c0)/(a1*b0-a0*b1);
      return Node(x, y);
    }
  }

  final double coords (int i) const pure nothrow @safe @nogc {
    switch (i) {
      case 0: return x1;
      case 1: return y1;
      case 2: return x2;
      case 3: return y2;
      default:
    }
    return 0.0;
  }

  final double distanceLine() (in auto ref Node node) const nothrow @trusted @nogc {
    import std.math : abs, sqrt;
    double a = x2-x1;
    double b = y2-y1;
    double bunbo = sqrt(a*a+b*b);
    double bunshi = a*(node.y-y1)-b*(node.x-x1);
    if (bunbo == 0.0) {
      //{ import core.stdc.stdio : stderr, fprintf; stderr.fprintf("ERROR: error in Segment.distanceLine()\n"); }
      return 0.0;
    }
    return abs(bunshi/bunbo);
  }

  final bool sameLine (in Segment segment) const nothrow @safe @nogc {
    return online(segment.startNode) && online(segment.endNode);
  }

  final Segment coordSystem (in Vector2 normal) const nothrow @safe {
    double _x1 = x1*normal.x+y1*normal.y;
    double _y1 = -x1*normal.y+y1*normal.x;
    double _x2 = x2*normal.x+y2*normal.y;
    double _y2 = -x2*normal.y+y2*normal.x;
    return new Segment(_x1, _y1, _x2, _y2);
  }

  final bool cross (in Segment s) const nothrow @safe @nogc {
    double a0 = y1-y2;
    double b0 = x2-x1;
    double c0 = y2*x1-x2*y1;
    double a1 = s.y1-s.y2;
    double b1 = s.x2-s.x1;
    double c1 = s.y2*s.x1-s.x2*s.y1;
    return (a0*s.x1+b0*s.y1+c0)*(a0*s.x2+b0*s.y2+c0) <= ErrorThreshold && (a1*x1+b1*y1+c1)*(a1*x2+b1*y2+c1) <= ErrorThreshold;
  }

  final void configure() (in auto ref Node start, in auto ref Node end) nothrow @safe @nogc {
    assert(start.valid);
    assert(end.valid);
    x1 = start.x;
    y1 = start.y;
    x2 = end.x;
    y2 = end.y;
  }

  final double distance() (in auto ref Node node) const nothrow @safe @nogc {
    immutable Node start = startNode;
    immutable Node end = endNode;
    Vector2 vec0 = Vector2(start, end);
    Vector2 vec1 = Vector2(start, node);
    if (vec0.innerProduct(vec1) < 0.0) return getDistance(node, start);
    vec1 = Vector2(end, node);
    if (vec0.innerProduct(vec1) > 0.0) return getDistance(node, end);
    return distanceLine(node);
  }

static:
  double getDistance() (in auto ref Node p, in auto ref Node q) nothrow @safe @nogc {
    import std.math : sqrt;
    return sqrt((p.x-q.x)*(p.x-q.x)+(p.y-q.y)*(p.y-q.y));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
class Segments {
public:
  Segment[] segments;

protected:
  void save (VFile fl) /*const*/ {
    fl.writeNum!uint(cast(uint)segments.length);
    foreach (Segment seg; segments) seg.save(fl);
  }

public:
  this () {}

  void clear () { segments.unsafeArrayClear(); }

  final bool appendSeg (Segment segment) {
    assert(segment !is null);
    if (neglible(segment.length)) return false;
    // debug
    foreach (Segment sg; segments[]) assert(sg !is segment);
    // debug end
    segments.unsafeArrayAppend(segment);
    return true;
  }

  final Segment getClosest (double x, double y, double min) nothrow @nogc {
    double current = min;
    Node point = Node(x, y);
    Segment result = null;
    foreach (Segment segment; segments) {
      double distance = segment.distance(point);
      if (current == distance && result !is null) {
        if (segment.length < result.length) result = segment;
      } else if (current >= distance) {
        current = distance;
        result = segment;
      }
    }
    return result;
  }

  final bool containIdenticalSegment (Segment segment) {
    if (segment is null) return false;
    foreach (Segment s; segments) if (segment.same(s)) return true;
    return false;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class Constraint {
public:
  enum Type {
    INVALID = 0,
    START_NODE, /*1*/
    END_NODE, /*2*/
    CONGRUENT, /*3*/
    ALIGN_X1, /*4*/
    ALIGN_Y1, /*5*/
    ALIGN_X2, /*6*/
    ALIGN_Y2, /*7*/
    START_ONLINE, /*8*/
    END_ONLINE, /*9*/
    SLOPE, /*10*/
    DIFF_X, /*11*/
    DIFF_Y, /*12*/
    ALIGN_X, /*13*/
    ALIGN_Y, /*14*/
    PARALLEL, /*15*/
  }

private:
  static immutable Type[4] ALIGN = [Type.ALIGN_X1, Type.ALIGN_Y1, Type.ALIGN_X2, Type.ALIGN_Y2];

public:
  public Type type;
  public int coeffCount;
  public double[3] coefficients;
  public double deviation;
  public Segment[] references;

public:
  this (Type t, double c, double adeviation) nothrow {
    type = t;
    coeffCount = 1;
    coefficients[0] = c;
    deviation = adeviation;
  }

  this (Type t, double x, double y, double adeviation) nothrow {
    type = t;
    coeffCount = 2;
    coefficients[0] = x;
    coefficients[1] = y;
    deviation = adeviation;
  }

  this (Type t, Node node, double adeviation) nothrow {
    type = t;
    coeffCount = 2;
    coefficients[0] = node.x;
    coefficients[1] = node.y;
    deviation = adeviation;
  }

  this (Type t, double a, double b, double c, double adeviation) nothrow {
    type = t;
    coeffCount = 3;
    coefficients[0] = a;
    coefficients[1] = b;
    coefficients[2] = c;
    deviation = adeviation;
  }

  void referencesAppend (Segment element) {
    if (element !is null) references.unsafeArrayAppend(element);
  }

  bool equal (in Constraint c) nothrow @safe @nogc {
    if (type != c.type) return false;
    if (type == Type.SLOPE) return true;
    assert(coeffCount == c.coeffCount);
    //foreach (immutable int i; 0..coeffCount) if (coefficients[i] != c.coefficients[i]) return false;
    //return true;
    return (coefficients[0..coeffCount] == c.coefficients[0..coeffCount]);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Constraints {
private:
  Constraint[] constraints;

public:
  this () nothrow @safe @nogc {}

  void addCongruent (double x, double y, double deviation, Segment segment) {
    Constraint constraint = new Constraint(Constraint.Type.CONGRUENT, x, y, deviation);
    checkAndAppend(constraint, segment);
  }

  void addStartOnline (Segment segment, double deviation) {
    Constraint constraint;
    if (segment.x1 == segment.x2) {
      constraint = new Constraint(Constraint.Type.ALIGN_X1, segment.x1, deviation);
    } else if (segment.y1 == segment.y2) {
      constraint = new Constraint(Constraint.Type.ALIGN_Y1, segment.y1, deviation);
    } else {
      double a = segment.y2-segment.y1;
      double b = segment.x1-segment.x2;
      double c = segment.x1*segment.y2-segment.y1*segment.x2;
      constraint = new Constraint(Constraint.Type.START_ONLINE, a, b, c, deviation);
    }
    checkAndAppend(constraint, segment);
  }

  void checkAndAppend (Constraint newConstraint, Segment segment) {
    foreach (immutable cidx, Constraint constraint; constraints) {
      if (constraint.type == newConstraint.type) {
        if (constraint.deviation+ErrorThreshold < newConstraint.deviation) return;
        if (constraint.deviation-ErrorThreshold < newConstraint.deviation) {
          if (constraint.equal(newConstraint)) constraint.referencesAppend(segment);
          return;
        } else {
          constraints.unsafeArrayRemove(cast(int)cidx);
          newConstraint.referencesAppend(segment);
          constraints.unsafeArrayAppend(newConstraint);
          return;
        }
      }
    }
    newConstraint.referencesAppend(segment);
    constraints.unsafeArrayAppend(newConstraint);
  }

  void addStartNode (Node node, double deviation, Segment segment) {
    Constraint constraint = new Constraint(Constraint.Type.START_NODE, node, deviation);
    checkAndAppend(constraint, segment);
  }

  void addEndOnline (Segment segment, double deviation) {
    Constraint constraint;
    if (segment.x1 == segment.x2) {
      constraint = new Constraint(Constraint.Type.ALIGN_X2, segment.x1, deviation);
    } else if (segment.y1 == segment.y2) {
      constraint = new Constraint(Constraint.Type.ALIGN_Y2, segment.y1, deviation);
    } else {
      double a = segment.y2-segment.y1;
      double b = segment.x1-segment.x2;
      double c = segment.x1*segment.y2-segment.y1*segment.x2;
      constraint = new Constraint(Constraint.Type.END_ONLINE, a, b, c, deviation);
    }
    checkAndAppend(constraint, segment);
  }

  void addSlope (double a, double b, double deviation, Segment segment) {
    Constraint constraint;
         if (a == 0.0) constraint = new Constraint(Constraint.Type.DIFF_Y, 0.0, deviation);
    else if (b == 0.0) constraint = new Constraint(Constraint.Type.DIFF_X, 0.0, deviation);
    else constraint = new Constraint(Constraint.Type.SLOPE, a, b, deviation);
    checkAndAppend(constraint, segment);
  }

  void addEndNode (Node node, double deviation, Segment segment) {
    Constraint constraint = new Constraint(Constraint.Type.END_NODE, node, deviation);
    checkAndAppend(constraint, segment);
  }

  void addDiffX (double c) {
    constraints.unsafeArrayAppend(new Constraint(Constraint.Type.DIFF_X, 0.0, 0.0));
  }

  void addAlign (Constraint.Type type, double constant, double deviation, Segment segment) {
    Constraint constraint = new Constraint(type, constant, deviation);
    checkAndAppend(constraint, segment);
  }

  void addDiffY (double c) {
    constraints.unsafeArrayAppend(new Constraint(Constraint.Type.DIFF_Y, 0.0, 0.0));
  }

  void addParallel (Segment segment, double interval, double deviation) {
    Constraint constraint;
    if (segment.x1 == segment.x2) {
      constraint = new Constraint(Constraint.Type.ALIGN_X, segment.x1-sign(segment.y2-segment.y1)*interval, deviation);
    } else if (segment.y1 == segment.y2) {
      constraint = new Constraint(Constraint.Type.ALIGN_Y, segment.y1+sign(segment.x2-segment.x1)*interval, deviation);
    } else {
      import std.math : sqrt;
      double a = segment.y2-segment.y1;
      double b = segment.x1-segment.x2;
      double c = segment.x1*segment.y2-segment.y1*segment.x2;
      c -= interval*sqrt(a*a+b*b);
      constraint = new Constraint(Constraint.Type.PARALLEL, a, b, c, deviation);
    }
    checkAndAppend(constraint, segment);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Element {
private:
  // constraint calculation result
  enum ConRes {
    SUCCEED, //0
    SUSPENDED, //1
    REDUNDANT, //2
    FAILED, //3
  }

  enum Relation {
    IDENTICAL, //0
    DIFFERENT, //1
    NOT_YET, //2
  }

  enum NONE = -1;
  enum X1 = 0;
  enum Y1 = 1;
  enum X2 = 2;
  enum Y2 = 3;

private:
  CloseEnough closeEnough;
  Element root;

public:
  double[4] variables = -1.0;
  bool complete;
  Constraint[] history;
  Constraint[] suspended;
  Element[] children;

private:
  Relation relation (Element e) const nothrow @safe @nogc {
    bool notYet = false;
    foreach (immutable int i; 0..4) {
      if (variables[i] != e.variables[i]) {
        if (e.fixed(i)) return Relation.DIFFERENT;
        notYet = true;
      }
    }
    if (notYet) return Relation.NOT_YET;
    return (e.suspended.length <= 0 ? Relation.IDENTICAL : Relation.DIFFERENT);
  }

  bool searchIdentical (Element element) nothrow {
    if (this is element) return false;
    switch (relation(element)) {
      case Relation.IDENTICAL:
        //element.history.merge(history);
        foreach (Constraint cc; history) element.history.unsafeArrayAppend(cc);
        return true;
      case Relation.DIFFERENT:
        return false;
      case Relation.NOT_YET:
        foreach (Element el; element.children) if (searchIdentical(el)) return true;
        break;
      default:
    }
    return false;
  }

  bool suspendCheck () {
    int sidx = 0;
    while (sidx < suspended.length) {
      Constraint constraint = suspended[sidx++];
      ConRes result = calculate(constraint);
      switch (result) {
        case ConRes.FAILED:
          return true;
        case ConRes.SUCCEED:
          //history.elements.append(constraint);
          //suspended.elements.remove(constraint);
          //e.reset();
          --sidx;
          history.unsafeArrayAppend(constraint);
          //k8: dunno, bugfix
          if (sidx >= 0 && sidx < suspended.length) suspended.unsafeArrayRemove(sidx);
          sidx = 0;
          break;
        case ConRes.REDUNDANT:
          //history.elements.append(constraint);
          //suspended.elements.remove(constraint);
          --sidx;
          history.unsafeArrayAppend(constraint);
          suspended.unsafeArrayRemove(sidx);
          break;
        default:
      }
    }
    return false;
  }

  ConRes calculateDiff (int i, int j, double constant) nothrow @safe @nogc {
    if (variables[i] == -1.0 && variables[j] == -1.0) return ConRes.SUSPENDED; //1
    if (variables[i] != -1.0 && variables[j] == -1.0) {
      variables[j] = variables[i]-constant;
      return ConRes.SUCCEED;
    }
    if (variables[i] == -1.0 && variables[j] != -1.0) {
      variables[i] = variables[j]+constant;
      return ConRes.SUCCEED;
    }
    return (!fpequal(variables[i]-variables[j], constant) ? ConRes.FAILED : ConRes.REDUNDANT);
  }

  static ConRes reportPariwiseConstraints (int i, int j) nothrow @safe @nogc {
    if (i == 3 || j == 3) return ConRes.FAILED; //3
    if (i == 2 && j == 2) return ConRes.REDUNDANT; //2
    return (i != 1 && j != 1 ? ConRes.SUCCEED : ConRes.SUSPENDED); // 0,1
  }

  ConRes calculateAlign (int i, double constant) nothrow @safe @nogc {
    if (variables[i] == constant) return ConRes.REDUNDANT; //2
    if (variables[i] == -1.0) {
      variables[i] = constant;
      return ConRes.SUCCEED; // 0
    }
    return ConRes.FAILED; //3
  }

  ConRes slopeAndOnline (double a, double b, double c, double aa, double bb, int x1, int y1, int x2, int y2) nothrow @safe @nogc {
    import std.math : abs;
    double diameter = a*bb-b*aa;
    if (abs(diameter) < ErrorThreshold) return ConRes.FAILED; //3
    immutable double tmp = variables[x2]*aa+variables[y2]*bb;
    variables[x1] = (-b*tmp+bb*c)/diameter;
    variables[y1] = (a*tmp-aa*c)/diameter;
    return ConRes.SUCCEED; //0
  }

  ConRes onlineAndOnline (double a0, double b0, double c0, double a1, double b1, double c1, int x1, int y1) nothrow @safe @nogc {
    import std.math : abs;
    //writeln("online, ", cast(int)a0, ", " cast(int)b0, ", " cast(int)c0, ", " cast(int)a1, ", " cast(int)b1, ", ", cast(int)c1);
    if (abs(a0*b1-a1*b0) < ErrorThreshold) return ConRes.FAILED; //3
    //writeln("succeed");
    variables[x1] = (-b0*c1+b1*c0)/(a0*b1-a1*b0);
    variables[y1] = (-a0*c1+a1*c0)/(a1*b0-a0*b1);
    return ConRes.SUCCEED; //0
  }

  ConRes calculateSlopeSub (Constraint constraint, int type, int x1, int y1, int x2, int y2) {
    foreach (immutable sidx, Constraint constraint2; suspended) {
      if (constraint2.type == type) {
        ConRes result = slopeAndOnline(constraint2.coefficients[0], constraint2.coefficients[1], constraint2.coefficients[2], constraint.coefficients[0], constraint.coefficients[1], x1, y1, x2, y2);
        if (result == ConRes.SUCCEED) {
          history.unsafeArrayAppend(constraint2);
          suspended.unsafeArrayRemove(cast(int)sidx);
        }
        return result;
      }
    }
    return ConRes.SUSPENDED;
  }

  ConRes calculateOnlineSub (int X1, int Y1, double a, double b, double c, int type) {
    int X2 = 3-Y1;
    int Y2 = 3-X1;
    foreach (immutable sidx, Constraint constraint2; suspended) {
      if (constraint2.type == type || constraint2.type == Constraint.Type.PARALLEL) {
        ConRes result = onlineAndOnline(a, b, c, constraint2.coefficients[0], constraint2.coefficients[1], constraint2.coefficients[2], X1, Y1);
        if (result == ConRes.SUCCEED && constraint2.type != Constraint.Type.PARALLEL) {
          history.unsafeArrayAppend(constraint2);
          suspended.unsafeArrayRemove(cast(int)sidx); // we'll get out of here anyway, so it is safe
        }
        if (result == ConRes.SUCCEED) return result;
      } else if (constraint2.type == Constraint.Type.SLOPE && fixed(X2) && fixed(Y2)) {
        ConRes result = slopeAndOnline(a, b, c, constraint2.coefficients[0], constraint2.coefficients[1], X1, Y1, X2, Y2);
        if (result == ConRes.SUCCEED) {
          history.unsafeArrayAppend(constraint2);
          suspended.unsafeArrayRemove(cast(int)sidx);
        }
        return result;
      }
    }
    return ConRes.SUSPENDED;
  }

  ConRes calculateOnline (int x, int y, double a, double b, double c, int type) {
    import std.math : abs;
    if (variables[x] == -1.0 && variables[y] == -1.0) return calculateOnlineSub(x, y, a, b, c, type);
    if (variables[x] == -1.0 && variables[y] != -1.0) {
      variables[x] = (c-variables[y]*b)/a;
      return ConRes.SUCCEED;
    }
    if (variables[y] == -1.0 && variables[x] != -1.0) {
      variables[y] = (c-variables[x]*a)/b;
      return ConRes.SUCCEED;
    }
    return (abs((variables[x]*a+variables[y]*b)-c) >= ErrorThreshold ? ConRes.FAILED : ConRes.REDUNDANT);
  }

  ConRes calculate (Constraint constraint) {
    auto v = variables[]; // cache; should be r/w linked to original (slice is)
    switch (constraint.type) {
      case Constraint.Type.START_NODE:
        if (v[0] == constraint.coefficients[0] && v[1] == constraint.coefficients[1]) return ConRes.REDUNDANT;
        if ((v[0] == -1.0 || v[0] == constraint.coefficients[0]) && (v[1] == -1.0 || v[1] == constraint.coefficients[1])) {
          v[0] = constraint.coefficients[0];
          v[1] = constraint.coefficients[1];
          return ConRes.SUCCEED;
        }
        return ConRes.FAILED;

      case Constraint.Type.END_NODE:
        if (v[2] == constraint.coefficients[0] && v[3] == constraint.coefficients[1]) return ConRes.REDUNDANT;
        if ((v[2] == -1.0 || v[2] == constraint.coefficients[0]) && (v[3] == -1.0 || v[3] == constraint.coefficients[1])) {
          v[2] = constraint.coefficients[0];
          v[3] = constraint.coefficients[1];
          return ConRes.SUCCEED;
        }
        return ConRes.FAILED;

      case Constraint.Type.CONGRUENT:
        int i = calculateDiff(2, 0, constraint.coefficients[0]);
        int j = calculateDiff(3, 1, constraint.coefficients[1]);
        return reportPariwiseConstraints(i, j);

      case Constraint.Type.ALIGN_X1:
        return calculateAlign(0, constraint.coefficients[0]);

      case Constraint.Type.ALIGN_Y1:
        return calculateAlign(1, constraint.coefficients[0]);

      case Constraint.Type.ALIGN_X2:
        return calculateAlign(2, constraint.coefficients[0]);

      case Constraint.Type.ALIGN_Y2:
        return calculateAlign(3, constraint.coefficients[0]);

      case Constraint.Type.START_ONLINE:
        return calculateOnline(0, 1, constraint.coefficients[0], constraint.coefficients[1], constraint.coefficients[2], 8);

      case Constraint.Type.END_ONLINE:
        return calculateOnline(2, 3, constraint.coefficients[0], constraint.coefficients[1], constraint.coefficients[2], 9);

      case Constraint.Type.DIFF_X:
        return calculateDiff(2, 0, constraint.coefficients[0]);

      case Constraint.Type.DIFF_Y:
        return calculateDiff(3, 1, constraint.coefficients[0]);

      case Constraint.Type.SLOPE:
        if (!fixed(0) && fixed(1) && fixed(2) && fixed(3)) {
          v[0] = v[2]+((v[3]-v[1])*constraint.coefficients[1])/constraint.coefficients[0];
          return ConRes.SUCCEED;
        }
        if (fixed(0) && !fixed(1) && fixed(2) && fixed(3)) {
          v[1] = v[3]+((v[2]-v[0])*constraint.coefficients[0])/constraint.coefficients[1];
          return ConRes.SUCCEED;
        }
        if (fixed(0) && fixed(1) && !fixed(2) && fixed(3)) {
          v[2] = v[0]-((v[3]-v[1])*constraint.coefficients[1])/constraint.coefficients[0];
          return ConRes.SUCCEED;
        }
        if (fixed(0) && fixed(1) && fixed(2) && !fixed(3)) {
          v[3] = v[1]-((v[2]-v[0])*constraint.coefficients[0])/constraint.coefficients[1];
          return ConRes.SUCCEED;
        }
        if (fixed(0) && fixed(1) && fixed(2) && fixed(3)) {
          //writeln((v[2] - v[0]) * constraint.coefficients[0] + (v[3] - v[1]) * constraint.coefficients[1]);
          return (!neglible((v[2]-v[0])*constraint.coefficients[0]+(v[3]-v[1])*constraint.coefficients[1]) ? ConRes.FAILED : ConRes.REDUNDANT);
        }
        if (!fixed(0) && !fixed(1) && fixed(2) && fixed(3)) return calculateSlopeSub(constraint, 8, 0, 1, 2, 3);
        if (fixed(0) && fixed(1) && !fixed(2) && !fixed(3)) return calculateSlopeSub(constraint, 9, 2, 3, 0, 1);
        return ConRes.SUSPENDED;

      case Constraint.Type.ALIGN_X:
        int i = calculateAlign(0, constraint.coefficients[0]);
        int j = calculateAlign(2, constraint.coefficients[0]);
        return reportPariwiseConstraints(i, j);

      case Constraint.Type.ALIGN_Y:
        int i = calculateAlign(1, constraint.coefficients[0]);
        int j = calculateAlign(3, constraint.coefficients[0]);
        return reportPariwiseConstraints(i, j);

      case Constraint.Type.PARALLEL:
        int i = calculateOnline(0, 1, constraint.coefficients[0], constraint.coefficients[1], constraint.coefficients[2], 8);
        int j = calculateOnline(2, 3, constraint.coefficients[0], constraint.coefficients[1], constraint.coefficients[2], 9);
        return reportPariwiseConstraints(i, j);

      default:
    }
    return ConRes.FAILED;
  }

  bool applyConstraintSub (Constraint constraint) {
    Element child = new Element(this);
    ConRes result = child.calculate(constraint);
    switch (result) {
      case ConRes.SUCCEED:
        if (!child.suspendCheck()) {
          child.history.unsafeArrayAppend(constraint);
          if (child.suspended.length == 0 && child.searchIdentical(root)) return true;
          child.checkComplete();
          closeEnough.check(child);
          children.unsafeArrayAppend(child);
          return true;
        }
        break;
      case ConRes.SUSPENDED:
        child.suspended.unsafeArrayAppend(constraint);
        children.unsafeArrayAppend(child);
        return true;
      case ConRes.REDUNDANT:
        history.unsafeArrayAppend(constraint);
        break;
      default:
    }
    return false;
  }

public:
  this (CloseEnough aCloseEnough) nothrow {
    root = this;
    closeEnough = aCloseEnough;
    complete = false;
    variables[] = -1.0; // just in case
  }

  this (Element e) nothrow {
    root = e.root;
    closeEnough = e.closeEnough;
    complete = false;
    variables = e.variables[];
    history = e.history.dup;
    suspended = e.suspended.dup;
  }

  void checkComplete () nothrow @safe @nogc {
    pragma(inline, true);
    if (variables[0] != -1.0 && variables[1] != -1.0 && variables[2] != -1.0 && variables[3] != -1.0) complete = true;
  }

  final bool fixed (int i) const pure nothrow @safe @nogc { pragma(inline, true); return (variables[i] != -1.0); }

  void applyConstraint (Constraint constraint) {
    auto originalChildren = children.dup;
    if (applyConstraintSub(constraint)) {
      foreach (Element el; originalChildren) el.applyConstraint(constraint);
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class Candidate : Segment {
  Constraint[] relatedConstraints;

  this (double _x1, double _y1, double _x2, double _y2, Constraint[] history) nothrow {
    super(_x1, _y1, _x2, _y2);
    relatedConstraints = history;
  }

  this (double _x1, double _y1, double _x2, double _y2) nothrow {
    super(_x1, _y1, _x2, _y2);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public final class Candidates : Segments {
private:
  Candidate primary;

public:
  Candidate getPrimaryCandidate () { pragma(inline, true); return primary; }
  Segment getPrimarySegment () { pragma(inline, true); return primary; }

  void setPrimary (Candidate target) { pragma(inline, true); primary = target; }

  override void clear () {
    super.clear();
    primary = null;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// namespace
public struct Beautifier {
  @disable this ();
  @disable this (this);

static:
  Candidates generateCandidates (Segment stroke, Scene scene, double size) {
    Constraints constraints = Inferencer.inferConstraints(stroke, scene, size);
    Candidates candidates = Solver.solveConstraints(constraints, stroke, size);
    return evaluate(candidates);
  }

  Segment beautify (Segment stroke, Scene scene) {
    return new Segment(stroke.x1+10.0, stroke.y1, stroke.x2+10.0, stroke.y2);
  }

  // Evaluator
  private Candidates evaluate (Candidates candidates) {
    Candidate primary = (candidates.segments.length ? cast(Candidate)candidates.segments[0] : null);
    //Candidate primary = cast(Candidate)candidates.elements.front;
    candidates.setPrimary(primary);
    return candidates;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
// namespace
struct Inferencer {
  @disable this ();
  @disable this (this);

static:
public:
  Constraints inferConstraints (Segment stroke, Scene scene, double scale) {
    initialize(scale);
    constraints = new Constraints();
    double xdiff = stroke.x2-stroke.x1;
    double ydiff = stroke.y2-stroke.y1;
    startNodeIsConnected = false;
    endNodeIsConnected = false;
    foreach (Segment segment; scene.segments) {
      checkConnection(stroke, segment, scene);
      checkAlign(stroke, segment);
      checkSlope(stroke, segment);
      checkCongruent(xdiff, ydiff, segment);
    }
    if (startNodeIsConnected) removeAlign(4, 5);
    if (endNodeIsConnected) removeAlign(6, 7);
    checkHorizontalVertical(stroke);
    return constraints;
  }

private:
  void initialize (double size) {
    MINIMUM_ALIGN = _MINIMUM_ALIGN/size;
    MINIMUM_CONGRUENCE = _MINIMUM_CONGRUENCE/size;
    MINIMUM_CONNECT = _MINIMUM_CONNECT/size;
    MINIMUM_TOUCH = _MINIMUM_TOUCH/size;
    MINIMUM_PARALLEL = _MINIMUM_PARALLEL/size;
    MAX_PARALLEL = _MAX_PARALLEL/size;
  }

  void checkCongruentSub (double xdiff, double ydiff, double oldXdiff, double oldYdiff, Segment segment) {
    import std.math : abs;
    double deviation = Vector2.distance(abs(xdiff), abs(ydiff), oldXdiff, oldYdiff);
    if (deviation < MINIMUM_CONGRUENCE) {
      constraints.addCongruent(sign(xdiff)*oldXdiff, sign(ydiff)*oldYdiff, deviation, segment);
    }
  }

  void checkCongruent (double xdiff, double ydiff, Segment segment) {
    import std.math : abs;
    double oldXdiff = abs(segment.x2-segment.x1);
    double oldYdiff = abs(segment.y2-segment.y1);
    checkCongruentSub(xdiff, ydiff, oldXdiff, oldYdiff, segment);
    checkCongruentSub(xdiff, ydiff, oldYdiff, oldXdiff, segment);
  }

  void checkAlignSub (int i, double constant, Segment stroke, Segment segment) {
    import std.math : abs;
    double deviation = abs(stroke.coords(i)-constant);
    if (deviation < MINIMUM_ALIGN) constraints.addAlign(Constraint.ALIGN[i], constant, deviation, segment);
  }

  void checkAlign (Segment stroke, Segment segment) {
    checkAlignSub(X1, segment.x1, stroke, segment);
    checkAlignSub(X1, segment.x2, stroke, segment);
    checkAlignSub(Y1, segment.y1, stroke, segment);
    checkAlignSub(Y1, segment.y2, stroke, segment);
    checkAlignSub(X2, segment.x1, stroke, segment);
    checkAlignSub(X2, segment.x2, stroke, segment);
    checkAlignSub(Y2, segment.y1, stroke, segment);
    checkAlignSub(Y2, segment.y2, stroke, segment);
    if (fpequal(segment.x1, segment.x2)) {
      double midY = (segment.y1+segment.y2)/2.0;
      checkAlignSub(Y1, midY, stroke, segment);
      checkAlignSub(Y2, midY, stroke, segment);
    } else if (fpequal(segment.y1, segment.y2)) {
      double midX = (segment.x1+segment.x2)/2.0;
      checkAlignSub(X1, midX, stroke, segment);
      checkAlignSub(X2, midX, stroke, segment);
    }
  }

  void checkHorizontalVertical (Segment stroke) {
    import std.math : abs, sqrt;
    immutable Vector2 newVector = stroke.vector();
    enum double r3 = sqrt(3.0);
    static immutable double[16] vectors = [1.0, 0, r3, 1.0, 1.0, 1.0, 1.0, r3, 0, 1.0, -1.0, r3, -1.0, 1.0, -r3, 1.0];
    foreach (immutable int i; 0..8) {
      Vector2 vector = Vector2(vectors[i*2], vectors[i*2+1]);
      immutable double deviation = abs(newVector.getSin(vector));
      if (deviation < MINIMUM_SLOPE) {
        constraints.addSlope(vector.y, -vector.x, deviation, null);
        return;
      }
    }
  }

  void checkSlope (Segment stroke, Segment segment) {
    import std.math : abs;
    immutable Vector2 newVector = stroke.vector();
    immutable Vector2 oldVector = segment.vector();
    double deviation = abs(oldVector.getSin(newVector));
    if (deviation < MINIMUM_SLOPE) {
      constraints.addSlope(segment.y2-segment.y1, segment.x1-segment.x2, deviation, segment);
      return;
    }
    deviation = abs(oldVector.getCos(newVector));
    if (deviation < MINIMUM_SLOPE) constraints.addSlope(segment.x2-segment.x1, segment.y2-segment.y1, deviation, segment);
  }

  void checkConnection (Segment stroke, Segment segment, Scene scene) {
    import std.math : abs;
    double deviation = Node.distance(stroke.startNode, segment.startNode);
    if (deviation < MINIMUM_CONNECT) {
      startNodeIsConnected = true;
      constraints.addStartNode(segment.startNode, deviation, segment);
      return;
    }
    deviation = Node.distance(stroke.startNode, segment.endNode);
    if (deviation < MINIMUM_CONNECT) {
      startNodeIsConnected = true;
      constraints.addStartNode(segment.endNode, deviation, segment);
      return;
    }
    deviation = Node.distance(stroke.endNode, segment.startNode);
    if (deviation < MINIMUM_CONNECT) {
      endNodeIsConnected = true;
      constraints.addEndNode(segment.startNode, deviation, segment);
      return;
    }
    deviation = Node.distance(stroke.endNode, segment.endNode);
    if (deviation < MINIMUM_CONNECT) {
      endNodeIsConnected = true;
      constraints.addEndNode(segment.endNode, deviation, segment);
      return;
    }
    deviation = segment.distance(stroke.startNode);
    if (deviation < MINIMUM_TOUCH) {
      constraints.addStartOnline(segment, deviation);
      return;
    }
    deviation = segment.distance(stroke.endNode);
    if (deviation < MINIMUM_TOUCH) {
      constraints.addEndOnline(segment, deviation);
      return;
    }
    immutable Vector2 newVector = stroke.vector();
    immutable Vector2 oldVector = segment.vector();
    deviation = abs(oldVector.getSin(newVector));
    if (deviation < MINIMUM_SLOPE) {
      immutable Vector2 normalVectorX = oldVector.normalize();
      Segment stroke2 = stroke.coordSystem(normalVectorX);
      Segment segment2 = segment.coordSystem(normalVectorX);
      immutable double interval = stroke2.y1-segment2.y1;
      if (abs(interval) < MINIMUM_ALIGN) {
        constraints.addParallel(segment, 0.0, abs(deviation));
        return;
      }
      if (abs(interval) > MAX_PARALLEL) return;
      if ((stroke2.x1-segment2.x2)*(stroke2.x2-segment2.x1) < 0.0 || (stroke2.x1-segment2.x2)*(stroke2.x2-segment2.x2) < 0.0 || (segment2.x1-stroke2.x1)*(segment2.x2-stroke2.x1) < 0.0) {
        immutable double target = scene.getClosestInterval(abs(interval));
        deviation = abs(target-abs(interval));
        if (deviation < MINIMUM_PARALLEL) constraints.addParallel(segment, sign(interval)*target, abs(deviation));
      }
    }
  }

  void removeAlign (int type1, int type2) {
    int cidx = 0;
    while (cidx < constraints.constraints.length) {
      Constraint constraint = constraints.constraints[cidx];
      if (constraint.type == type1 || constraint.type == type2) constraints.constraints.unsafeArrayRemove(cidx); else ++cidx;
    }
  }

  enum X1 = 0;
  enum Y1 = 1;
  enum X2 = 2;
  enum Y2 = 3;
  enum NONE = 0;
  enum CONNECT_SS = 1;
  enum CONNECT_SE = 2;
  enum CONNECT_ES = 3;
  enum CONNECT_EE = 4;
  enum TOUCH_START = 5;
  enum TOUCH_END = 6;
  enum _MINIMUM_ALIGN = 10.0;
  enum _MINIMUM_CONGRUENCE = 30.0;
  enum _MINIMUM_CONNECT = 20.0;
  enum _MINIMUM_TOUCH = 10.0;
  enum _MINIMUM_PARALLEL = 30.0;
  enum _MAX_PARALLEL = 200.0;
  enum MINIMUM_SLOPE = 0.10000000000000001;

  double MINIMUM_ALIGN;
  double MINIMUM_CONGRUENCE;
  double MINIMUM_CONNECT;
  double MINIMUM_TOUCH;
  double MINIMUM_PARALLEL;
  double MAX_PARALLEL;
  Constraints constraints;
  bool startNodeIsConnected;
  bool endNodeIsConnected;
}


// ////////////////////////////////////////////////////////////////////////// //
// namespace
struct Solver {
  @disable this ();
  @disable this (this);

static:
public:
  Candidates solveConstraints (Constraints constraints, Segment stroke, double size) {
    Element root = generateSolutionTree(constraints, stroke, size);
    Candidates candidates = gatherCompleteElements(root);
    //writeln("# of candidates = ", candidates.size);
    return candidates;
  }

private:
  Element generateSolutionTree (Constraints constraints, Segment stroke, double size) {
    auto closeEnough = CloseEnough(stroke, size);
    Element root = new Element(closeEnough);
    //for (auto e = constraints.elements(); !e.empty; root.apply_constraint(e.nextElement())) {}
    foreach (Constraint cc; constraints.constraints) root.applyConstraint(cc);
    foreach (immutable int i; 0..4) {
      if (closeEnough.notFound(i)) {
        //writeln("not close ", i);
        root.applyConstraint(new Constraint(Constraint.ALIGN[i], stroke.coords(i), 0.0));
      }
    }
    return root;
  }

  Candidates gatherCompleteElements (Element root) {
    Candidates candidates = new Candidates();
    candidates = gatherCompleteElementsSub(root, candidates);
    return candidates;
  }

  Candidates gatherCompleteElementsSub (Element element, Candidates candidates) {
    if (element.complete) {
      Candidate segment = new Candidate(element.variables[0], element.variables[1], element.variables[2], element.variables[3], element.history);
      candidates.appendSeg(segment);
    }
    foreach (Element el; element.children) candidates = gatherCompleteElementsSub(el, candidates);
    return candidates;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class Context {
public:
  double x, y;

public nothrow @safe @nogc:
  this (Segment connected, Segment reference, int refCxtRelation, int trgRefRelation) {
    immutable Node cs = connected.startNode;
    immutable Node ce = connected.endNode;
    immutable Node rs = reference.startNode;
    immutable Node re = reference.endNode;

    switch (refCxtRelation) {
      case Predictor.CONNECT_SS:
        x = ce.x-cs.x;
        y = ce.y-cs.y;
        break;
      case Predictor.CONNECT_SE:
        x = cs.x-ce.x;
        y = cs.y-ce.y;
        break;
      case Predictor.CONNECT_ES:
        x = -(ce.x-cs.x);
        y = -(ce.y-cs.y);
        break;
      case Predictor.CONNECT_EE:
        x = -(cs.x-ce.x);
        y = -(cs.y-ce.y);
        break;
      default:
    }

    switch (trgRefRelation) {
      case Predictor.VERTICAL_AXIS:
        x = -x;
        break;
      case Predictor.HORIZONTAL_AXIS:
        y = -y;
        break;
      case Predictor.REVERSE:
        x = -x;
        y = -y;
        break;
      default:
    }
  }

  bool equal (in Context c) const { pragma(inline, true); return fpequal(x, c.x) && fpequal(y, c.y); }
}


// ////////////////////////////////////////////////////////////////////////// //
// namespace
public struct Predictor {
  @disable this ();
  @disable this (this);

static:
private:
  Context[] contexts;
  Candidates candidates;
  Scene scene;

public:
  enum NONE = -1;
  enum NORMAL = 0;
  enum VERTICAL_AXIS = 1;
  enum HORIZONTAL_AXIS = 2;
  enum REVERSE = 3;

  enum CONNECT_SS = 1;
  enum CONNECT_SE = 2;
  enum CONNECT_ES = 3;
  enum CONNECT_EE = 4;

public:
  Candidates generateCandidates (Segment trigger, Scene ascene) {
    if (ascene is null) return null;
    candidates = new Candidates();
    scene = ascene;
    // for GC
    scope(exit) {
      scene = null;
      candidates = null;
      contexts = null;
    }
    foreach (Segment reference; scene.segments) {
      int trgRefRelation = checkCongruent(trigger, reference);
      if (trgRefRelation != -1) addContexts(trigger, reference, trgRefRelation);
    }
    foreach (Context ctx; contexts) addPredicted(trigger, ctx);
    return candidates;
  }

private:
  int checkCongruent (Segment trigger, Segment reference) nothrow @safe @nogc {
    immutable Vector2 t = trigger.vector();
    immutable Vector2 r = reference.vector();
    if (fpequal(t.x, r.x) && fpequal(t.y, r.y)) return NORMAL;
    if (fpequal(t.x, -r.x) && fpequal(t.y, r.y)) return VERTICAL_AXIS;
    if (fpequal(t.x, r.x) && fpequal(t.y, -r.y)) return HORIZONTAL_AXIS;
    return (!fpequal(t.x, -r.x) || !fpequal(t.y, -r.y) ? NONE : REVERSE);
  }

  void addPredictedSub (Node node, double x, double y) {
    Candidate candidate = new Candidate(node.x, node.y, node.x+x, node.y+y);
    if (!scene.containIdenticalSegment(candidate)) candidates.appendSeg(candidate);
  }

  void addPredicted (Segment trigger, Context context) {
    addPredictedSub(trigger.startNode, context.x, context.y);
    addPredictedSub(trigger.endNode, -context.x, -context.y);
    if (fpequal(trigger.x1, trigger.x2)) {
      addPredictedSub(trigger.startNode, -context.x, context.y);
      addPredictedSub(trigger.endNode, context.x, -context.y);
    } else if (fpequal(trigger.y1, trigger.y2)) {
      addPredictedSub(trigger.startNode, context.x, -context.y);
      addPredictedSub(trigger.endNode, -context.x, context.y);
    }
  }

  void addContext (Segment connected, Segment reference, int refCxtRelation, int trgRefRelation) {
    Context newContext = new Context(connected, reference, refCxtRelation, trgRefRelation);
    foreach (Context context; contexts) if (newContext.equal(context)) return;
    contexts.unsafeArrayAppend(newContext);
  }

  void addContexts (Segment trigger, Segment reference, int trgRefRelation) {
    foreach (Segment connected; scene.segments) {
      int refCxtRelation = checkConnected(reference, connected);
      if (refCxtRelation != NONE) addContext(connected, reference, refCxtRelation, trgRefRelation);
    }
  }

  int checkConnected (Segment reference, Segment connected) nothrow @safe @nogc {
    immutable Node rs = reference.startNode;
    immutable Node re = reference.endNode;
    immutable Node cs = connected.startNode;
    immutable Node ce = connected.endNode;
    if (rs.same(cs)) return CONNECT_SS;
    if (rs.same(ce)) return CONNECT_SE;
    if (re.same(cs)) return CONNECT_ES;
    return (!re.same(ce) ? NONE : CONNECT_EE);
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public class Scene : Segments {
private:
  // ////////////////////////////////////////////////////////////////////// //
  // this is used in segments merging and dividing, but not simultaneously; let's reuse it
  Node[] ndlist;
  bool ndlistlocked; // just in case

  void ndlistLock () {
    if (ndlistlocked) assert(0, "internal error");
    ndlistlocked = true;
    if (ndlist.length != 0) { ndlist[] = null; ndlist.length = 0; ndlist.assumeSafeAppend; }
  }

  void ndlistUnlock () {
    if (!ndlistlocked) assert(0, "internal error");
    ndlistlocked = false;
    if (ndlist.length != 0) { ndlist.length = 0; ndlist.assumeSafeAppend; }
  }

  void ndlistAppend() (auto ref Node n) {
    if (!ndlistlocked) assert(0, "internal error");
    ndlist.unsafeArrayAppend(n);
  }

  void ndlistSort () {
    import std.algorithm : sort;
    if (!ndlistlocked) assert(0, "internal error");
    if (ndlist.length < 2) return;
    ndlist.sort!((Node node1, Node node2) {
      if (fpequal(node1.y, node2.y)) return (node1.x < node2.x);
      return (node1.y < node2.y);
    });
  }

  // ////////////////////////////////////////////////////////////////////// //
  static final class SegmentPair {
    Segment segment1, segment2;

    this (Segment s0, Segment s1) nothrow @safe @nogc {
      pragma(inline, true);
      segment1 = s0;
      segment2 = s1;
    }

    bool member (Segment segment) const nothrow @safe @nogc { pragma(inline, true); return (segment1 is segment || segment2 is segment); }

    override string toString () const {
      import std.format : format;
      if (segment1 !is null && segment2 !is null) return "<%s,%s>".format(segment1, segment2);
      if (segment1 !is null) return "<%s,null>".format(segment1);
      if (segment2 !is null) return "<null,%s>".format(segment2);
      return "<null,null>";
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  static final class Interval {
    double value = 0;
    SegmentPair[] owners;

    this (double d, Segment segment, Segment segment1) {
      import std.math : isNaN;
      assert(segment !is null);
      assert(segment1 !is null);
      assert(segment !is segment1);
      assert(!d.isNaN);
      value = d;
      owners.unsafeArrayAppend(new SegmentPair(segment, segment1));
    }

    void addOwner (Segment segment, Segment segment1) {
      //pragma(inline, true);
      assert(segment !is null);
      assert(segment1 !is null);
      assert(segment !is segment1);
      // debug
      version(pegasus_interval_checks) {
        foreach (SegmentPair sp; owners) {
          if (sp.member(segment) && sp.member(segment1)) {
            conwriteln("WARNING: duplicate segment in interval");
            return;
          }
        }
      }
      // end debug
      owners.unsafeArrayAppend(new SegmentPair(segment, segment1));
    }

    bool removeOwner (Segment segment) {
      int oidx = 0;
      while (oidx < owners.length) {
        if (owners[oidx].member(segment)) owners.unsafeArrayRemove(oidx); else ++oidx;
      }
      return (owners.length == 0);
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
private:
  Interval[] intervals;

  bool findSegWithId (ulong id) {
    foreach (Segment seg; segments) {
      if (seg.id == id) return true;
    }
    return false;
  }

  void checkIntervals () {
    version(pegasus_interval_checks) {
      double prevev = -double.infinity;
      foreach (immutable eidx, Interval e; intervals) {
        if (e is null) { conwriteln("!!!!!!!!!! eidx=", eidx); assert(0, "WTF?!"); }
        if (prevev > e.value) {
          conwriteln("interval #", eidx, "; value=", e.value, "; count=", e.owners.length, ": fucked value; prev=", prevev, "; value=", e.value);
        }
        prevev = e.value;
        foreach (SegmentPair sp; e.owners) {
          if (sp.segment1 !is null) {
            if (sp.segment2 is null) {
              conwriteln("interval; value=", e.value, "; count=", e.owners.length, ": seg2 is null");
              assert(0, "fuuuu");
            }
            if (!findSegWithId(sp.segment1.id)) {
              conwriteln("interval; value=", e.value, "; count=", e.owners.length, ": no seg1");
              assert(0, "fuuuu");
            }
            if (!findSegWithId(sp.segment2.id)) {
              conwriteln("interval; value=", e.value, "; count=", e.owners.length, ": no seg2");
              assert(0, "fuuuu");
            }
          } else {
            if (sp.segment1 !is null) {
              conwriteln("interval; value=", e.value, "; count=", e.owners.length, ": seg1 is not null");
              assert(0, "fuuuu");
            }
          }
        }
      }
    }
  }

private:
  double getClosestInterval (double interval) {
    import std.math : isNaN;
    assert(!interval.isNaN);
    double min = interval*2.0;
    double result = 0;
    foreach (Interval ev; intervals) {
      import std.math : abs;
      immutable double value = ev.value;
      immutable double abv = abs(interval-value);
      if (min > abv) {
        min = abv;
        result = value;
      } else {
        return result;
      }
    }
    return result;
  }

  void addNewInterval (double interval, Segment segment, Segment segment2) {
    import std.math : isNaN;
    assert(segment !is null);
    assert(segment2 !is null);
    assert(segment !is segment2);
    assert(!interval.isNaN);
    int iidx = 0;
    while (iidx < intervals.length) {
      Interval existingInterval = intervals[iidx];
      immutable double value = existingInterval.value;
      if (fpequal(interval, value)) {
        existingInterval.addOwner(segment, segment2);
        return;
      }
      if (interval < value) {
        intervals.unsafeArrayInsertBefore(iidx, new Interval(interval, segment, segment2));
        return;
      }
      ++iidx;
    }
    intervals.unsafeArrayAppend(new Interval(interval, segment, segment2));
  }

  void updateIntervals (Segment segment) {
    assert(segment !is null);
    foreach (Segment segment2; segments) {
      if (segment2 is segment) continue;
      if (segment2.parallel(segment)) {
        double interval = segment2.parallelInterval(segment);
        if (interval >= 0) addNewInterval(interval, segment, segment2);
      }
    }
  }

  void appendSegAndFixIntervals (Segment segment) {
    assert(segment !is null);
    // inserting already splitted segments (from loaded scene, for example) can produce segments with zero size; let's be safe here
    if (segment.length < 2) {
      //conwriteln("dropped out tiny segment: ", segment, " (", segment.id, "; len=", segment.length, ")");
      return;
    }
    updateIntervals(segment);
    segments.unsafeArrayAppend(segment);
  }

  void removeIntervals (Segment segment) {
    int iidx = 0;
    while (iidx < intervals.length) {
      if (intervals[iidx].removeOwner(segment)) intervals.unsafeArrayRemove(iidx); else ++iidx;
    }
  }

  Segment mergeSegments (Segment segment) {
    assert(segment !is null);
    int sidx = 0;
    while (sidx < segments.length) {
      Segment oldSegment = segments[sidx];
      // just in case
      if (oldSegment is segment) {
        removeIntervals(oldSegment);
        segments.unsafeArrayRemove(sidx);
        continue;
      }
      if (segment.sameLine(oldSegment) &&
          (segment.between(oldSegment.startNode) ||
           segment.between(oldSegment.endNode) ||
           oldSegment.between(segment.startNode) ||
           oldSegment.between(segment.endNode)))
      {
        {
          ndlistLock();
          scope(exit) ndlistUnlock();
          ndlistAppend(oldSegment.startNode);
          ndlistAppend(oldSegment.endNode);
          ndlistAppend(segment.startNode);
          ndlistAppend(segment.endNode);
          ndlistSort();
          //conwriteln("nodelist sorted: head=", ndlist[0], "; tail=", ndlist[$-1]);
          segment = new Segment(ndlist[0], ndlist[$-1]);
        }
        //_remove();
        removeIntervals(oldSegment);
        segments.unsafeArrayRemove(sidx);
      } else {
        ++sidx;
      }
    }
    return segment;
  }

  void divideSegments (Segment segment) {
    assert(segment !is null);
    Segments createdSegments = new Segments();
    ndlistLock();
    scope(exit) ndlistUnlock();
    ndlistAppend(segment.startNode);
    ndlistAppend(segment.endNode);
    int sidx = 0;
    while (sidx < segments.length) {
      Segment oldSegment = segments[sidx];
      // just in case
      if (oldSegment is segment) {
        removeIntervals(oldSegment);
        segments.unsafeArrayRemove(sidx);
        continue;
      }
      if (!segment.cross(oldSegment)) { ++sidx; continue; }
      Node crossNode = segment.crossNode(oldSegment);
      if (!crossNode.valid) { ++sidx; continue; }
      if (!crossNode.same(oldSegment.startNode) && !crossNode.same(oldSegment.endNode)) {
        //_remove();
        removeIntervals(oldSegment);
        segments.unsafeArrayRemove(sidx);
        //
        createdSegments.appendSeg(new Segment(oldSegment.startNode, crossNode));
        createdSegments.appendSeg(new Segment(oldSegment.endNode, crossNode));
      } else {
        ++sidx;
      }
      if (!crossNode.same(segment.startNode) && !crossNode.same(segment.endNode)) ndlistAppend(crossNode);
    }
    foreach (Segment cs; createdSegments.segments) appendSegAndFixIntervals(cs);
    ndlistSort();
    assert(ndlist.length > 1);
    foreach (immutable idx; 1..ndlist.length) appendSegAndFixIntervals(new Segment(ndlist[idx-1], ndlist[idx]));
  }

  void mergeSegmentsAfterRemoveSub() (Segments connectedSegments, in auto ref Node node) {
    if (connectedSegments.segments.length != 2) return;
    Segment segment1 = connectedSegments.segments[0];
    Segment segment2 = connectedSegments.segments[1];
    if (!segment1.sameLine(segment2)) return;
    immutable Node start = (node.same(segment1.startNode) ? segment1.endNode : segment1.startNode);
    immutable Node end = (node.same(segment2.startNode) ? segment2.endNode : segment2.startNode);
    //segments.elements.remove(segment1);
    //segments.elements.remove(segment2);
    int sidx = 0;
    while (sidx < segments.length) {
      auto cseg = segments[sidx];
      if (cseg is segment1 || cseg is segment2) {
        segments.unsafeArrayRemove(sidx);
      } else {
        ++sidx;
      }
    }
    removeIntervals(segment1);
    removeIntervals(segment2);
    appendSegAndFixIntervals(new Segment(start, end));
  }

  void mergeSegmentsAfterRemove (Segment segment) {
    assert(segment !is null);
    immutable Node start = segment.startNode;
    immutable Node end = segment.endNode;
    Segments connectedToStart = new Segments();
    Segments connectedToEnd = new Segments();
    foreach (Segment oldSegment; segments) {
           if (start.same(oldSegment.startNode) || start.same(oldSegment.endNode)) connectedToStart.appendSeg(oldSegment);
      else if (end.same(oldSegment.startNode) || end.same(oldSegment.endNode)) connectedToEnd.appendSeg(oldSegment);
    }
    mergeSegmentsAfterRemoveSub(connectedToStart, start);
    mergeSegmentsAfterRemoveSub(connectedToEnd, end);
  }

public:
  this () {
  }

  override void clear () {
    super.clear();
    intervals.unsafeArrayClear();
  }

  void append (Segment segment) {
    if (segment is null) return;
    foreach (Segment seg; segments) {
      if (seg is segment) { conwriteln("WARNING: refused to add duplicate segment"); return; }
      if (seg.id == segment.id) { conwriteln("WARNING: refused to add segment with duplicate id"); return; }
    }
    if (neglible(segment.length)) return; // just in case
    Segment mergedSegment = mergeSegments(segment);
    divideSegments(mergedSegment);
    checkIntervals();
  }

  // returns removed segment or `null`
  // `checkCB` should return `true` if segment should be removed
  Segment removeFirstSegWith (scope bool delegate (Segment seg) checkCB) {
    if (checkCB is null) assert(0, "check delegate?");
    int sidx = 0;
    while (sidx < segments.length) {
      if (checkCB(segments[sidx])) {
        Segment seg = segments[sidx];
        removeIntervals(seg);
        segments.unsafeArrayRemove(sidx);
        mergeSegmentsAfterRemove(seg);
        checkIntervals();
        return seg;
      }
      ++sidx;
    }
    return null;
  }

  bool remove (Segment segment) {
    if (segment is null) return false;
    return (removeFirstSegWith((Segment seg) => seg is segment) !is null);
  }

  // ////////////////////////////////////////////////////////////////////// //
  void dumpIntervals () {
    uint iidx = 0;
    foreach (Interval iv; intervals) {
      conwriteln("=== interval #", iidx++, " (", iv.owners.length, " pairs, value=", iv.value, ") ===");
      uint sidx = 0;
      foreach (SegmentPair sp; iv.owners) {
        conwriteln("  #", sidx++, ": ", sp);
      }
    }
    conwriteln("===========================");
  }

  void regenIntervals () {
    if (segments.length == 0) return;
    auto oldsegs = segments;
    segments = null; // or `clear()` will wipe it
    dumpIntervals();
    clear();
    foreach (immutable sidx, Segment seg; oldsegs) {
      conwriteln(" #", sidx, ": ", *cast(void**)&seg);
      conwriteln("  seg: ", seg, " (", seg.id, "; len=", seg.length, ")");
      append(seg);
    }
    conwriteln("<<<<<<<<<<<<<<<<<<<<<<<<<<< NEW >>>>>>>>>>>>>>>>>>>>>>>>>>>");
    dumpIntervals();
  }

public:
  // ////////////////////////////////////////////////////////////////////// //
  enum Signature = "PEGARIP1";

  void saveScene (VFile fl) {
    fl.rawWriteExact(Signature[]);
    super.save(fl); // this saves segment list
    // no need to save intervals, they will be rebuilt
  }

  void loadScene (VFile fl) {
    clear();
    scope(failure) clear();
    char[Signature.length] sign;
    fl.rawReadExact(sign[]);
    if (sign[0..$-1] != Signature[0..$-1]) throw new Exception("invalid scene file signature");
    ubyte ver = 0;
    switch (sign[$-1]) {
      case '0': ver = 0; break;
      case '1': ver = 1; break;
      default: throw new Exception("invalid scene file version");
    }
    //super.load(ver, fl);
    // this loads segment list; need to do it manually, 'cause we need to append each segment to rebuild intervals
    uint count = fl.readNum!uint;
    if (count >= uint.max/8) throw new Exception("too many segments in list");
    foreach (immutable c; 0..count) {
      auto seg = Segment.loadNew(ver, fl);
      if (seg is null) continue;
      append(seg);
    }
    // don't load interval list, it should be already rebuilt
    if (ver == 0) conwriteln("pegasus scene loader: ignored interval data");
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public abstract class InteractiveScene {
private import core.time;

public:
  enum MinimumStrokeLength = 10;
  enum MinimumSelectDistance = 10;
  enum WireVisible = 2;
  enum DefaultScale = 0.8;

  enum Operation {
    None,
    Zooming,
    Moving,
    Drawing,
    Erasing,
  }

  enum State {
    Settled,
    Selecting,
    Predicting,
  }

  enum LINES = 0;
  enum POINTS = 1;

public:
  PegPainter wpa;

  Operation operation = Operation.None;
  State state = State.Settled;

  Scene scene;
  Candidates candidates;
  Eraser eraser;
  Feedback feedback;

  int x1, y1, x2, y2;
  int xl, yl;

  double strokeLength = 0;
  double scale = DefaultScale;
  double originalScale = DefaultScale;
  int frameX = 0, frameY = 0;
  int originalFrameX, originalFrameY;

  bool regenBackground;
  bool redrawBackground;

  Segment[] wires;

protected:
  XPixmap backimg;

protected:
  // ////////////////////////////////////////////////////////////////////// //
  void wiresReset () {
    wires.unsafeArrayClear();
  }

  void wireAppend (Segment s) {
    if (s is null) return;
    wires.unsafeArrayAppend(s);
  }

  // ////////////////////////////////////////////////////////////////////// //
  static struct ClickCountInfo {
    MonoTime lastClick;
    int count;
  }

  ClickCountInfo[3] clickCount; // l,m,r

  int getClickCount (MouseButton b) {
    int bidx;
    switch (b) {
      case MouseButton.left: bidx = 0; break;
      case MouseButton.middle: bidx = 1; break;
      case MouseButton.right: bidx = 2; break;
      default: return 0;
    }
    return clickCount[bidx].count;
  }

  public void clearClickCounts () {
    clickCount[] = ClickCountInfo.init;
  }

  void processClickCounts (MouseEvent e) {
    auto ct = MonoTime.currTime;
    foreach (ref ClickCountInfo ci; clickCount[]) {
      auto dif = (ct-ci.lastClick).total!"msecs";
      if (dif > 300) {
        ci.lastClick = MonoTime.zero;
        ci.count = 0;
      }
    }
    int bidx;
    switch (e.button) {
      case MouseButton.left: bidx = 0; break;
      case MouseButton.middle: bidx = 1; break;
      case MouseButton.right: bidx = 2; break;
      default: return;
    }
    if (e.type == MouseEventType.buttonPressed) {
      clickCount[bidx].lastClick = ct;
      ++clickCount[bidx].count;
    } else if (e.type == MouseEventType.buttonReleased) {
      //lastClick = ct;
      //++clickCount[bidx];
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  final double s2cSize (double size) const pure nothrow @safe @nogc { pragma(inline, true); return size/scale; }

  // from screen to canvas
  final double s2cX (int x) const pure nothrow @safe @nogc { pragma(inline, true); return cast(double)(x-frameX)/scale; }
  final double s2cY (int y) const pure nothrow @safe @nogc { pragma(inline, true); return cast(double)(y-frameY)/scale; }

  // from canvas to screen
  final int c2sX (double x) const pure nothrow @safe @nogc { pragma(inline, true); return cast(int)(x*scale)+frameX; }
  final int c2sY (double y) const pure nothrow @safe @nogc { pragma(inline, true); return cast(int)(y*scale)+frameY; }

  static struct screenXY { int x, y; }
  static struct canvasXY { double x, y; }

  final screenXY c2sXY (double x, double y) const pure nothrow @safe @nogc { pragma(inline, true); return screenXY(cast(int)(x*scale)+frameX, cast(int)(y*scale)+frameY); }
  final screenXY c2sXY() (in auto ref canvasXY xy) const pure nothrow @safe @nogc { pragma(inline, true); return screenXY(cast(int)(xy.x*scale)+frameX, cast(int)(xy.y*scale)+frameY); }

  final canvasXY s2cXY (int x, int y) const pure nothrow @safe @nogc { pragma(inline, true); return canvasXY(cast(double)(x-frameX)/scale, cast(double)(y-frameY)/scale); }
  final canvasXY s2cXY() (in auto ref screenXY xy) const pure nothrow @safe @nogc { pragma(inline, true); return canvasXY(cast(double)(xy.x-frameX)/scale, cast(double)(xy.y-frameY)/scale); }

  // ////////////////////////////////////////////////////////////////////// //
  final class Eraser {
    int size = 5;
    Timer timer;

    void doErase () {
      Node erasePoint = Node(s2cX(x2), s2cY(y2));
      double convertedSize = s2cSize(size);
      bool removed = false;
      while (scene.removeFirstSegWith(delegate (Segment segment) => segment.distance(erasePoint) < convertedSize)) removed = true;
      if (removed) regen(); else refresh();
    }

    void interim (MouseEvent e) {
      delete timer;
      x2 = e.x;
      y2 = e.y;
      doErase();
      x1 = x2;
      y1 = y2;
    }

    void start (MouseEvent e) {
      size = 5;
      delete timer;
      timer = new Timer(200, delegate () {
        size = cast(int)(cast(double)size*1.4);
        doErase();
      });
      operation = Operation.Erasing;
      mouseStart(e);
      feedback.drawInit();
      doErase();
    }

    void eraseClosest (int x, int y) {
      if (scene.segments.length == 0) return;
      Node erasePoint = Node(s2cX(x), s2cY(y));
      Segment closest = scene.segments[0];
      double min = closest.distance(erasePoint);
      foreach (Segment segment; scene.segments[1..$]) {
        if (segment.distance(erasePoint) < min) {
          closest = segment;
          min = segment.distance(erasePoint);
        }
      }
      scene.remove(closest);
    }

    void finish (MouseEvent e) {
      delete timer;
    }
  }

  // ////////////////////////////////////////////////////////////////////// //
  final class Feedback {
    int prevX, prevY;

    void drawInit () {
      prevX = x1;
      prevY = y1;
    }

    void draw () {
      wpa.colorFG = Color.red;
      if (operation == Operation.Drawing) {
        immutable int p1 = prevX;
        immutable int p2 = prevY;
        immutable int p3 = x2;
        immutable int p4 = y2;
        immutable double p5 = scale*8;
        prevX = x2;
        prevY = y2;
        wpa.drawWideLine(p1, p2, p3, p4, p5);
        if (scale > WireVisible) wpa.drawLine(p1, p2, p3, p4);
      } else if (operation == Operation.Erasing) {
        immutable int p1 = x1-eraser.size;
        immutable int p2 = y1-eraser.size;
        immutable int p3 = eraser.size*2;
        immutable int p4 = eraser.size*2;
        wpa.drawEllipse(p1, p2, p3, p4);
      }
    }
  }

public:
  this (PegPainter awpa) {
    wpa = awpa;
    clear();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void clear () {
    regenBackground = true;
    redrawBackground = true;
    eraser = new Eraser();
    feedback = new Feedback();
    if (scene is null) scene = new Scene(); else scene.clear();
    candidates = new Candidates();
    operation = Operation.None;
    state = State.Settled;
    strokeLength = 0;
    scale = originalScale = DefaultScale;
    frameX = frameY = 0;
    feedback.drawInit();
    clearClickCounts();
  }

  void save (VFile fl) {
    scene.saveScene(fl);
  }

  void load (VFile fl) {
    scope(exit) regen();
    clear();
    scope(failure) clear();
    scene.loadScene(fl);
  }

  // ////////////////////////////////////////////////////////////////////// //
  void onMouseEvent (MouseEvent e) {
    processClickCounts(e);
    if (e.type == MouseEventType.motion && (e.modifierState&(ModifierState.leftButtonDown|ModifierState.middleButtonDown|ModifierState.rightButtonDown)) != 0) {
      mouseDragged(e);
      return;
    }
    if (e.type == MouseEventType.buttonPressed) { mousePressed(e); return; }
    if (e.type == MouseEventType.buttonReleased) { mouseReleased(e); return; }
  }

  // ////////////////////////////////////////////////////////////////////// //
  void mouseDragged (MouseEvent e) {
    switch (operation) {
      case Operation.Zooming: zoomingInterim(e); break;
      case Operation.Moving: movingInterim(e); break;
      case Operation.Erasing: eraser.interim(e); break;
      case Operation.Drawing: drawingInterim(e); break;
      default:
    }
  }

  void mousePressed (MouseEvent e) {
    //conwriteln("button=", e.button, "; clickcount=", getClickCount(e.button));
    switch (e.button) {
      case MouseButton.right:
        if (getClickCount(MouseButton.right) == 2) zoomingStart(e); else movingStart(e);
        break;
      case MouseButton.left:
        if (getClickCount(MouseButton.left) == 2) {
               if (state == State.Selecting) finishSelection();
          else if (state == State.Predicting) interruptPrediction();
        } else {
          drawingStart(e);
        }
        break;
      case MouseButton.middle:
        eraser.start(e);
        break;
      case MouseButton.wheelUp:
      case MouseButton.wheelDown:
        if (operation == Operation.Erasing) {
          eraser.interim(e);
          eraser.size = cast(int)(e.button == MouseButton.wheelUp ? eraser.size*1.4 : eraser.size/1.4);
          if (eraser.size < 5) eraser.size = 5;
          eraser.doErase();
        } else if (operation == Operation.None) {
          modifyScale(e.x, e.y, e.button == MouseButton.wheelUp ? 1 : -1);
        }
        break;
      default:
    }
  }

  void mouseReleased (MouseEvent e) {
    switch (operation) {
      case Operation.Zooming:
      case Operation.Moving:
        if (e.button != MouseButton.right) return;
        break;
      case Operation.Erasing:
        if (e.button != MouseButton.middle) return;
        eraser.finish(e);
        break;
      case Operation.Drawing:
        if (e.button != MouseButton.left) return;
        if (Vector2.distance(x1, y1, x2, y2) > MinimumStrokeLength) {
          if (state == State.Selecting) finishSelection();
          drawingFinish(e);
          break;
        }
        switch (state) {
          case State.Selecting: selectCandidate(); break;
          case State.Predicting: selectPredictedCandidate(); break;
          case State.Settled: startPrediction(); break;
          default:
        }
        break;
      default:
    }
    operation = Operation.None;
    regen();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void mouseStart (MouseEvent e) {
    x1 = e.x;
    y1 = e.y;
    x2 = x1;
    y2 = y1;
  }

  void movingStart (MouseEvent e) {
    operation = Operation.Moving;
    mouseStart(e);
  }

  void movingInterim (MouseEvent e) {
    frameX += e.x-x1;
    frameY += e.y-y1;
    x1 = e.x;
    y1 = e.y;
    regen();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void zoomingStart (MouseEvent e) {
    operation = Operation.Zooming;
    originalScale = scale;
    originalFrameX = frameX;
    originalFrameY = frameY;
    mouseStart(e);
  }

  // cx,cy: screen point that should stay in place
  void modifyScale (int cx, int cy, double v) {
    import std.math : pow, E;
    // get canvas point for screen point
    canvasXY cxy = s2cXY(cx, cy);
    // change scale
    scale = scale*pow(E, v/10); // so scale will progress smoothly
    if (scale < 0.05) scale = 0.05;
    // now make sure that the given canvas point will return to it's place
    screenXY nxy = c2sXY(cxy);
    frameX += cx-nxy.x;
    frameY += cy-nxy.y;
    // done
    regen();
  }

  void zoomingInterim (MouseEvent e) {
    import std.math : pow, E;
    scale = originalScale*pow(E, cast(double)(e.x-x1)/100);
    if (scale < 0.05) scale = 0.05;
    frameX = x1+cast(int)((cast(double)(originalFrameX-x1)*scale)/originalScale);
    frameY = y1+cast(int)((cast(double)(originalFrameY-y1)*scale)/originalScale);
    regen();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void generatePredictions (Segment trigger) {
    candidates = Predictor.generateCandidates(trigger, scene);
    if (candidates.segments.length > 0) state = State.Predicting; else state = State.Settled;
    regen();
  }

  void startPrediction () {
    Segment trigger = scene.getClosest(s2cX(x1), s2cY(y1), MinimumSelectDistance);
    if (trigger !is null) generatePredictions(trigger);
  }

  void interruptPrediction () {
    candidates.clear();
    state = State.Settled;
    regen();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void drawingStart (MouseEvent e) {
    operation = Operation.Drawing;
    mouseStart(e);
    strokeLength = 0;
    feedback.drawInit();
    refresh();
  }

  void drawingInterim (MouseEvent e) {
    import std.math : sqrt;
    xl = x2;
    yl = y2;
    x2 = e.x;
    y2 = e.y;
    strokeLength += sqrt(cast(double)(x2-xl)*(x2-xl)+(y2-yl)*(y2-yl));
    updateFeedback();
  }

  void drawingFinish (MouseEvent e) {
    import std.math : sqrt;
    //System.out.println("storke " + stroke_length + " " + Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) / stroke_length);
    if (strokeLength < MinimumStrokeLength) return;
    if (sqrt(cast(double)(x2-x1)*(x2-x1)+(y2-y1)*(y2-y1))/strokeLength < 0.6) {
      // erase segment with scratching
      finishSelection();
      eraser.eraseClosest(x1, y1);
    } else {
      Segment stroke = new Segment(s2cX(x1), s2cY(y1), s2cX(x2), s2cY(y2));
      candidates = Beautifier.generateCandidates(stroke, scene, scale);
      state = State.Selecting;
    }
    update();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void selectCandidate () {
    Segment segment = candidates.getClosest(s2cX(x1), s2cY(y1), MinimumSelectDistance);
    if (segment is null) {
      Segment trigger = candidates.getPrimarySegment();
      finishSelection();
      if (trigger !is null) generatePredictions(trigger);
    } else {
      candidates.setPrimary(cast(Candidate)segment);
      regen();
    }
  }

  void selectPredictedCandidate () {
    Segment segment = candidates.getClosest(s2cX(x1), s2cY(y1), MinimumSelectDistance);
    if (segment !is null) {
      scene.append(segment);
      segment = scene.getClosest(s2cX(x1), s2cY(y1), MinimumSelectDistance);
    }
    if (segment is null) {
      interruptPrediction();
    } else {
      generatePredictions(segment);
    }
  }

  void finishSelection () {
    if (candidates.segments.length > 0 && state != State.Predicting) {
      Segment primary = candidates.getPrimarySegment();
      if (primary !is null) scene.append(primary);
    }
    candidates.clear();
    state = State.Settled;
    regen();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void drawExplanation (Candidate c, Constraint constraint) {
    void draw_explanation_align_x (double x, Segment[] references) {
      foreach (Segment s; references) {
        if (s.coords(0) == x) wpa.drawVerticalMark(c2sX(s.x1), c2sY(s.y1));
             if (s.coords(2) == x) wpa.drawVerticalMark(c2sX(s.x2), c2sY(s.y2));
        else if ((s.coords(0)+s.coords(2))/2 == x) wpa.drawVerticalMark(c2sX((s.x2+s.x1)/2), c2sY((s.y2+s.y1)/2));
      }
    }

    void draw_explanation_align_y (double y, Segment[] references) {
      foreach (Segment s; references) {
        if (s.coords(1) == y) wpa.drawHorizontalMark(c2sX(s.x1), c2sY(s.y1));
             if (s.coords(3) == y) wpa.drawHorizontalMark(c2sX(s.x2), c2sY(s.y2));
        else if ((s.coords(1)+s.coords(3))/2 == y) wpa.drawHorizontalMark(c2sX((s.x2+s.x1)/2), c2sY((s.y2+s.y1)/2));
      }
    }

    switch (constraint.type) {
      case Constraint.Type.DIFF_X:
      case Constraint.Type.DIFF_Y:
      default:
        break;

      case Constraint.Type.CONGRUENT:
        foreach (Segment s; constraint.references) {
          wpa.drawCongruentMark(c2sX(s.x1), c2sY(s.y1), c2sX(s.x2), c2sY(s.y2));
        }
        break;

      case Constraint.Type.START_NODE:
      case Constraint.Type.START_ONLINE:
        wpa.drawWideCircle(c2sX(c.x1), c2sY(c.y1), wpa.MarkSize, 3);
        break;

      case Constraint.Type.END_NODE:
      case Constraint.Type.END_ONLINE:
        wpa.drawWideCircle(c2sX(c.x2), c2sY(c.y2), wpa.MarkSize, 3);
        break;

      case Constraint.Type.ALIGN_X1:
        if (constraint.references.length == 0) return;
        wpa.drawVerticalMark(c2sX(c.x1), c2sY(c.y1));
        draw_explanation_align_x(c.x1, constraint.references);
        break;

      case Constraint.Type.ALIGN_Y1:
        if (constraint.references.length == 0) return;
        wpa.drawHorizontalMark(c2sX(c.x1), c2sY(c.y1));
        draw_explanation_align_y(c.y1, constraint.references);
        break;

      case Constraint.Type.ALIGN_X2:
        if (constraint.references.length == 0) return;
        wpa.drawVerticalMark(c2sX(c.x2), c2sY(c.y2));
        draw_explanation_align_x(c.x2, constraint.references);
        break;

      case Constraint.Type.ALIGN_Y2:
        if (constraint.references.length == 0) return;
        wpa.drawHorizontalMark(c2sX(c.x2), c2sY(c.y2));
        draw_explanation_align_y(c.y2, constraint.references);
        break;

      case Constraint.Type.SLOPE:
        if (constraint.references.length == 0) {
          wpa.drawAngleMark(c2sX(c.x1), c2sY(c.y1), c2sX(c.x2), c2sY(c.y2));
        } else {
          foreach (Segment s; constraint.references) {
            if (s.parallel(c)) {
              wpa.drawSlopeMark(c2sX(s.x1), c2sY(s.y1), c2sX(s.x2), c2sY(s.y2));
            } else {
              wpa.drawSlopeMark2(c2sX(s.x1), c2sY(s.y1), c2sX(s.x2), c2sY(s.y2));
            }
          }
        }
        break;

      case Constraint.Type.ALIGN_X:
      case Constraint.Type.ALIGN_Y:
      case Constraint.Type.PARALLEL:
        assert(constraint.references.length > 0);
        Segment s = constraint.references[0];
        double min = c.parallelInterval(s);
        foreach (Segment segment; constraint.references[1..$]) {
          double interval = c.parallelInterval(segment);
          if (min > interval) {
            min = interval;
            s = segment;
          }
        }
        Node midNode = Node((c.x1+c.x2)/2, (c.y1+c.y2)/2);
        Segment n = new Segment(midNode.x, midNode.y, midNode.x-(c.y2-c.y1), midNode.y+(c.x2-c.x1));
        Node crossNode = n.crossNode(s);
        if (!crossNode.valid) {
          conwriteln("error in parallel explanation");
        } else {
          n = new Segment(midNode, crossNode);
          wpa.drawParallelMark(c2sX(n.x1), c2sY(n.y1), c2sX(n.x2), c2sY(n.y2));
        }
        break;
    }
  }

  void drawExplanations (Candidate candidate) {
    wpa.colorFG = Color.green;
    foreach (Constraint ct; candidate.relatedConstraints) drawExplanation(candidate, ct);
  }

  void paint () {
    void drawSegmentInit () {
      wiresReset();
    }

    void drawSegment (Segment p, Color color) {
      wpa.colorFG = color;
      wpa.drawWideLine(c2sX(p.x1), c2sY(p.y1), c2sX(p.x2), c2sY(p.y2), scale*8);
      wpa.drawLine(c2sX(p.x1), c2sY(p.y1), c2sX(p.x2), c2sY(p.y2));
      if (scale > WireVisible) wireAppend(p);
    }

    void drawSegmentWires () {
      if (wires.length == 0) return; // just in case
      wpa.colorFG = Color.white;
      foreach (Segment p; wires) wpa.drawLine(c2sX(p.x1), c2sY(p.y1), c2sX(p.x2), c2sY(p.y2));
    }

    if (regenBackground) {
      wpa.colorFG = Color.white;
      wpa.cls();
      drawSegmentInit();
      foreach (Segment s; scene.segments) drawSegment(s, Color.black);
      foreach (Segment s; candidates.segments) drawSegment(s, Color(255, 0, 255));
      Candidate primary = candidates.getPrimaryCandidate();
      if (primary !is null) drawSegment(primary, Color.red);
      drawSegmentWires();
      if (primary !is null) drawExplanations(primary);
      wpa.copyToPixmap(backimg);
      regenBackground = false;
      redrawBackground = false;
    }

    if (redrawBackground) {
      wpa.blit(backimg);
      redrawBackground = false;
    }

    feedback.draw();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void refresh () {
    redrawBackground = true; // erase feedback, but don't rebuild background
    update();
  }

  void regen () {
    regenBackground = true;
    update();
  }

  void updateFeedback () {
    update();
  }

  void update () {
    paint();
    wpa.flush();
  }

  // ////////////////////////////////////////////////////////////////////// //
  void regenIntervals () {
    scene.regenIntervals();
  }
}
