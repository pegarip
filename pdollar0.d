/*
 * The $P+ Point-Cloud Recognizer (JavaScript version)
 *
 *  Radu-Daniel Vatavu, Ph.D.
 *  University Stefan cel Mare of Suceava
 *  Suceava 720229, Romania
 *  vatavu@eed.usv.ro
 *
 * The academic publication for the $P+ recognizer, and what should be
 * used to cite it, is:
 *
 *     Vatavu, R.-D. (2017). Improving gesture recognition accuracy on
 *     touch screens for users with low vision. Proceedings of the ACM
 *     Conference on Human Factors in Computing Systems (CHI '17). Denver,
 *     Colorado (May 6-11, 2017). New York: ACM Press, pp. 4667-4679.
 *     https://dl.acm.org/citation.cfm?id=3025941
 *
 * This software is distributed under the "New BSD License" agreement:
 *
 * Copyright (C) 2017-2018, Radu-Daniel Vatavu and Jacob O. Wobbrock. All
 * rights reserved. Last updated July 14, 2018.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *    * Redistributions of source code must retain the above copyright
 *      notice, this list of conditions and the following disclaimer.
 *    * Redistributions in binary form must reproduce the above copyright
 *      notice, this list of conditions and the following disclaimer in the
 *      documentation and/or other materials provided with the distribution.
 *    * Neither the name of the University Stefan cel Mare of Suceava, nor the
 *      names of its contributors may be used to endorse or promote products
 *      derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Radu-Daniel Vatavu OR Lisa Anthony
 * OR Jacob O. Wobbrock BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT
 * OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */
module pdollar0;

// use $P instead of $P+?
// doesn't work right yet (something is wrong with the weights)
// maybe this is due to the different resampler?
// or due to the bug in angle calculation?
version = use_older_dollar_p;

import iv.alice;
import iv.cmdcon;
import iv.vfs;


// ////////////////////////////////////////////////////////////////////////// //
struct DPResult {
  string name;
  float score;
  @property bool valid () const nothrow @safe @nogc { pragma(inline, true); import std.math : isNaN; return !score.isNaN; }
}


// ////////////////////////////////////////////////////////////////////////// //
struct DPPoint {
  float x=0.0f, y=0.0f;
  uint id = 0; // stroke ID to which this point belongs (1,2,...)
  version(use_older_dollar_p) {} else {
    float angle=0.0f;
  }
}


final class DPPointCloud {
public:
  enum NumPoints = 32;

public:
  string name;
  DPPoint[NumPoints] points = DPPoint(0.0f, 0.0f);

private:
  this () {}

public:
  this (string aname, const(DPPoint)[] pts...) nothrow @nogc {
    name = aname;
    resample(points, pts);
    scale(points);
    translateTo(points, DPPoint(0.0f, 0.0f));
    version(use_older_dollar_p) {} else computeNormalizedTurningAngles(points);
  }

  void save (VFile fl) const {
    string nn = name;
    if (nn.length > 65535) nn = nn[0..65535]; // fuck you
    if (nn.length > 254) {
      fl.writeNum!ubyte(255);
      fl.writeNum!ushort(cast(ushort)nn.length);
    } else {
      fl.writeNum!ubyte(cast(ubyte)nn.length);
    }
    fl.rawWriteExact(nn);
    fl.writeNum!ubyte(NumPoints);
    foreach (const ref DPPoint pt; points[]) {
      fl.writeNum!float(cast(float)pt.x);
      fl.writeNum!float(cast(float)pt.y);
      fl.writeNum!uint(pt.id);
    }
  }

  void load (VFile fl) {
    char[] nn;
    ubyte len = fl.readNum!ubyte;
    if (len == 255) {
      ushort xlen = fl.readNum!ushort;
      nn.length = xlen;
    } else {
      nn.length = len;
    }
    fl.rawReadExact(nn);
    name = cast(string)nn; // it is safe to cast here
    if (fl.readNum!ubyte != NumPoints) throw new Exception("invalid number of points in cloud");
    foreach (ref DPPoint pt; points[]) {
      pt.x = cast(float)fl.readNum!float;
      pt.y = cast(float)fl.readNum!float;
      pt.id = fl.readNum!uint;
    }
    // perform last $P+ step
    version(use_older_dollar_p) {} else computeNormalizedTurningAngles(points);
  }

  static DPPointCloud loadNew (VFile fl) {
    auto res = new DPPointCloud();
    res.load(fl);
    return res;
  }
}


// ////////////////////////////////////////////////////////////////////////// //
final class DPGestureList {
private:
  DPPointCloud[] gestures;

public:
  this () nothrow @nogc {}

  void clear () {
    delete gestures;
  }

  string[] knownGestureNames () {
    string[] res;
    mainloop: foreach (DPPointCloud gst; gestures) {
      foreach (string s; res) if (s == gst.name) continue mainloop;
      res ~= gst.name;
    }
    return res;
  }

  void appendGesture (string aname, const(DPPoint)[] pts...) {
    appendGesture(new DPPointCloud(aname, pts));
  }

  // you can add as many gestures with same name as you want to
  void appendGesture (DPPointCloud gg) {
    import core.memory : GC;
    if (gg is null) return;
    auto optr = gestures.ptr;
    gestures ~= gg;
    if (gestures.ptr !is optr) {
      optr = gestures.ptr;
      if (optr is GC.addrOf(optr)) GC.setAttr(optr, GC.BlkAttr.NO_INTERIOR);
    }
  }

  // remove *ALL* gestures with the given name
  void removeGesture (const(char)[] name) {
    usize idx = 0;
    while (idx < gestures.length) {
      if (gestures[idx].name == name) {
        foreach (immutable c; idx+1..gestures.length) gestures[c-1] = gestures[c];
        gestures[$-1] = null;
        gestures.length -= 1;
        gestures.assumeSafeAppend;
      } else {
        ++idx;
      }
    }
  }

  // if you have more than one gesture with the same name, keep increasing `idx` to get more, until you'll get `null`
  DPPointCloud findGesture (const(char)[] name, uint idx=0) nothrow @nogc {
    foreach (DPPointCloud g; gestures) {
      if (g.name == name) {
        if (idx-- == 0) return g;
      }
    }
    return null;
  }

  DPResult recognize (const(DPPoint)[] origpoints) nothrow @nogc {
    import std.algorithm : max;
    if (origpoints.length < 2) return DPResult.init;
    DPPoint[DPPointCloud.NumPoints] points;
    resample(points, origpoints);
    scale(points);
    translateTo(points, DPPoint(0.0f, 0.0f));
    version(use_older_dollar_p) {} else computeNormalizedTurningAngles(points);
    float b = float.infinity;
    int u = -1;
    foreach (immutable idx, DPPointCloud gst; gestures) {
      version(use_older_dollar_p) {
        immutable float d = greedyCloudMatch(points, gst);
      } else {
        immutable float d0 = cloudDistance(points, gst.points);
        immutable float d1 = cloudDistance(gst.points, points);
        immutable float d = (d0 < d1 ? d0 : d1);
      }
      if (d < b) {
        b = d; // best (least) distance
        u = cast(int)idx; // point-cloud
      }
    }
    if (u == -1) return DPResult();
    { import core.stdc.stdio; printf("b=%f (%f) (%f)\n", b, (b-2.0)/-2.0, (b > 1.0f ? 1.0f/b : 1.0f)); }
    version(use_older_dollar_p) {
      return (u != -1 ? DPResult(gestures[u].name, max((b-2.0f)/-2.0f, 0.0f)) : DPResult.init);
    } else {
      //return (u != -1 ? DPResult(gestures[u].name, max((b-2.0f)/-2.0f, 0.0f)) : DPResult.init);
      return (u != -1 ? DPResult(gestures[u].name, (b > 1.0f ? 1.0f/b : 1.0f)) : DPResult.init);
    }
  }

public:
  enum Signature = "DOLP8LB0";

  void save (VFile fl) const {
    fl.rawWriteExact(Signature);
    fl.writeNum!uint(cast(uint)gestures.length);
    foreach (const DPPointCloud gst; gestures) gst.save(fl);
  }

  void load (VFile fl) {
    delete gestures;
    char[Signature.length] sign;
    fl.rawReadExact(sign);
    if (sign[0..$-1] != Signature[0..$-1]) throw new Exception("invalid $P library signature");
    if (sign[$-1] != Signature[$-1]) throw new Exception("invalid $P library version");
    uint count = fl.readNum!uint;
    if (count > uint.max/16) throw new Exception("too many gestures in library");
    gestures.reserve(count);
    foreach (immutable idx; 0..count) appendGesture(DPPointCloud.loadNew(fl));
  }
}


// ////////////////////////////////////////////////////////////////////////// //
private:

version(use_older_dollar_p) {
  float greedyCloudMatch(size_t n) (in ref DPPoint[n] points, in DPPointCloud P) nothrow @nogc {
    import std.algorithm : min;
    import std.math : floor, pow;
    enum e = cast(float)0.50;
    int step = cast(int)floor(pow(points.length, 1-e));
    assert(step > 0);
    float minv = float.infinity;
    for (int i = 0; i < points.length; i += step) {
      auto d1 = cloudDistanceOld(points, P.points, i);
      auto d2 = cloudDistanceOld(P.points, points, i);
      minv = min(minv, d1, d2);
    }
    return minv;
  }

  float cloudDistanceOld(size_t n) (in ref DPPoint[n] pts1, in ref DPPoint[n] pts2, int start) nothrow @nogc {
    import std.math : sqrt;
    bool[n] matched = false;
    float sum = 0;
    int i = start;
    do {
      int index = -1;
      float minv = float.infinity;
      foreach (immutable j, bool mtv; matched[]) {
        if (!mtv) {
          auto d = distanceSqr(pts1[i], pts2[j]);
          if (d < minv) { minv = d; index = cast(int)j; }
        }
      }
      assert(index >= 0);
      matched[index] = true;
      float weight = cast(float)1-((i-start+cast(int)pts1.length)%cast(int)pts1.length)/cast(float)pts1.length;
      sum += weight*sqrt(minv);
      i = (i+1)%cast(int)pts1.length;
    } while (i != start);
    return sum;
  }
} else {
  // $P+
  float cloudDistance(size_t n) (in ref DPPoint[n] pts1, in ref DPPoint[n] pts2) nothrow @nogc {
    import std.math : sqrt;
    bool[n] matched = false;
    size_t mtcount = 0;
    float sum = 0.0f;

    foreach (immutable i; 0..pts1.length) {
      int index = -1;
      float minv = float.infinity;
      foreach (immutable j; 0..pts2.length) {
        immutable float d = distanceWithAngleSqr(pts1[i], pts2[j]);
        if (d < minv) {
          minv = d;
          index = cast(int)j;
        }
      }
      if (!matched[index]) {
        ++mtcount;
        matched[index] = true;
      }
      sum += cast(float)sqrt(minv);
    }

    if (mtcount != matched.length) {
      foreach (immutable j, bool mtv; matched[]) {
        if (!mtv) {
          float minv = float.infinity;
          foreach (immutable i; 0..pts1.length) {
            immutable float d = distanceWithAngleSqr(pts1[i], pts2[j]);
            if (d < minv) minv = d;
          }
          sum += cast(float)sqrt(minv);
        }
      }
    }

    return sum;
  }
}


void resample(size_t n) (ref DPPoint[n] newpoints, in DPPoint[] points) nothrow @nogc {
  import std.algorithm : max, min;
  import std.math : isNaN;
  assert(n > 0);
  assert(points.length > 1);
  immutable float I = pathLength(points)/(n-1); // interval length
  float D = 0.0f;
  uint nppos = 0;
  newpoints[nppos++] = points[0];
  foreach (immutable idx; 1..points.length) {
    if (points[idx].id == points[idx-1].id) {
      auto d = distance(points[idx-1], points[idx]);
      if (D+d >= I) {
        DPPoint firstPoint = points[idx-1];
        while (D+d >= I) {
          // add interpolated point
          float t = min(max((I-D)/d, 0.0f), 1.0f);
          if (isNaN(t)) t = 0.5f;
          newpoints[nppos++] = DPPoint((cast(float)1-t)*firstPoint.x+t*points[idx].x,
                                       (cast(float)1-t)*firstPoint.y+t*points[idx].y, points[idx].id);
          // update partial length
          d = D+d-I;
          D = 0.0f;
          firstPoint = newpoints[nppos-1];
        }
        D = d;
      } else {
        D += d;
      }
    }
  }
  if (nppos == n-1) {
    // sometimes we fall a rounding-error short of adding the last point, so add it if so
    newpoints[nppos++] = DPPoint(points[$-1].x, points[$-1].y, points[$-1].id);
  }
  assert(nppos == n);
}


void scale(size_t n) (ref DPPoint[n] points) nothrow @nogc {
  import std.algorithm : max, min;
  float minX = +float.infinity;
  float minY = +float.infinity;
  float maxX = -float.infinity;
  float maxY = -float.infinity;
  foreach (ref DPPoint p; points[]) {
    minX = min(minX, p.x);
    minY = min(minY, p.y);
    maxX = max(maxX, p.x);
    maxY = max(maxY, p.y);
  }
  immutable float size = max(maxX-minX, maxY-minY);
  foreach (ref DPPoint p; points[]) {
    p.x = (p.x-minX)/size;
    p.y = (p.y-minY)/size;
  }
}


// translates points' centroid
void translateTo(size_t n) (ref DPPoint[n] points, DPPoint pt) nothrow @nogc {
  auto c = centroid(points);
  foreach (ref DPPoint p; points[]) {
    p.x = p.x+pt.x-c.x;
    p.y = p.y+pt.y-c.y;
  }
}


DPPoint centroid(size_t n) (in ref DPPoint[n] points) nothrow @nogc {
  float x = 0.0f, y = 0.0f;
  foreach (const ref DPPoint p; points[]) {
    x += p.x;
    y += p.y;
  }
  immutable float pl = cast(float)1/cast(float)points.length;
  return DPPoint(x*pl, y*pl);
}


// modifies `points` angles
void computeNormalizedTurningAngles(size_t n) (ref DPPoint[n] points) nothrow @nogc {
  import std.math : acos, PI;
  points[0].angle = 0.0f;
  foreach (immutable idx; 1..n-1) {
    immutable float dx = (points[idx+1].x-points[idx].x)*(points[idx].x-points[idx-1].x);
    immutable float dy = (points[idx+1].y-points[idx].y)*(points[idx].y-points[idx-1].y);
    immutable float dn = distance(points[idx+1], points[idx])*distance(points[idx], points[idx-1]);
    //float cosangle = Math.max(-1.0, Math.min(1.0, (dx + dy) / dn)); // ensure [-1,+1]
    float cosangle = (dx+dy)/dn;
    // ensure [-1,+1]
    if (cosangle < -1.0f) cosangle = -1.0f; else if (cosangle > 1.0f) cosangle = 1.0f;
    immutable float angle = acos(cosangle)/cast(float)PI; // normalized angle
    points[idx].angle = angle;
  }
  // last point
  points[n-1].angle = 0.0f;
}


// length traversed by a point path
float pathLength (const(DPPoint)[] points) nothrow @nogc {
  float d = 0;
  foreach (immutable idx; 1..points.length) {
    if (points[idx].id == points[idx-1].id) d += distance(points[idx-1], points[idx]);
  }
  return d;
}


// Euclidean distance between two points
float distance() (in auto ref DPPoint p1, in auto ref DPPoint p2) nothrow @nogc {
  import std.math : sqrt;
  immutable float dx = p2.x-p1.x;
  immutable float dy = p2.y-p1.y;
  return cast(float)sqrt(dx*dx+dy*dy);
}


version(use_older_dollar_p) {
  // Euclidean distance between two points
  float distanceSqr() (in auto ref DPPoint p1, in auto ref DPPoint p2) nothrow @nogc {
    immutable float dx = p2.x-p1.x;
    immutable float dy = p2.y-p1.y;
    return cast(float)(dx*dx+dy*dy);
  }
} else {
  float distanceWithAngleSqr() (in auto ref DPPoint p1, in auto ref DPPoint p2) nothrow @nogc {
    import std.math : sqrt;
    immutable float dx = p2.x-p1.x;
    immutable float dy = p2.y-p1.y;
    immutable float da = p2.angle-p1.angle;
    //return cast(float)sqrt(dx*dx+dy*dy+da*da);
    return cast(float)(dx*dx+dy*dy+da*da);
  }
}
