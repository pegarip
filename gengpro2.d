/* Invisible Vector Library
 * coded by Ketmar // Invisible Vector <ketmar@ketmar.no-ip.org>
 * Understanding is not required. Only obedience.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3 of the License ONLY.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
// Protractor/Oleg Doperchuk's (improved) gesture recognizer, v2
module gengpro2 /*is aliced*/;
private:

import iv.alice;
import iv.nanpay;
import iv.vfs;

version = use_gesture_doperchuk;


// ////////////////////////////////////////////////////////////////////////// //
version(use_gesture_doperchuk) {
  public enum DoperchukImproved = true;
} else {
  public enum DoperchukImproved = false;
}


// ////////////////////////////////////////////////////////////////////////// //
/// gesture glyph (unistroke)
public class PTGlyph {
  // DO NOT CHANGE!
  version(use_gesture_doperchuk) {
    enum NormalizedPoints = 32; // dunno
  } else {
    enum NormalizedPoints = 16; // the paper says that this is enough for protractor to work ok
  }
  enum MaxNormalizedPoints = 64; // yet we will store up to this, to be compatible with $P and Doperchuk Improved
  static assert(NormalizedPoints > 2 && NormalizedPoints < ushort.max);
  enum MinPointDistance = 4;

  enum MinMatchScore = 1.5f; ///

private:
  alias PatArray = float[MaxNormalizedPoints*2U];

public:
  ///
  static struct Point {
    float x, y;
    @property bool valid () const pure nothrow @safe @nogc { pragma(inline, true); import iv.nanpay : isFiniteF; return (isFiniteF(x) && isFiniteF(y)); }
  }

private:
  PatArray patpointsNorm = void; // resampled, but not rotated
  PatArray patpoints = void; // resampled, rotated and vectorized
  uint patlength = 0; // number of *elements* in `patpoints`
  float[] points = null; // [0]:x, [1]:y, [2]:x, [3]:y, etc...
  bool mNormalized = false; // true: `patpointsNorm` and `patpoints` are ok
  bool mOriented = true;
  string mName = null;

private:
  static void unsafeArrayAppend(T) (ref T[] arr, auto ref T v) {
    auto optr = arr.ptr;
    arr ~= v;
    if (optr !is arr.ptr) {
      import core.memory : GC;
      optr = arr.ptr;
      if (optr !is null && optr is GC.addrOf(optr)) GC.setAttr(optr, GC.BlkAttr.NO_INTERIOR);
    }
  }

  static T[] unsafeArrayDup(T) (const(T)[] arr) {
    auto res = arr.dup;
    if (res.ptr) {
      import core.memory : GC;
      if (res.ptr !is null && res.ptr is GC.addrOf(res.ptr)) GC.setAttr(res.ptr, GC.BlkAttr.NO_INTERIOR);
    }
    return res;
  }

  static normBlkAttr(T) (T[] arr) {
    pragma(inline, true);
    import core.memory : GC;
    if (arr.ptr !is null && arr.ptr is GC.addrOf(arr.ptr)) GC.setAttr(arr.ptr, GC.BlkAttr.NO_INTERIOR);
  }

public:
  ///
  this () pure nothrow @safe @nogc {}

  ///
  this (string aname, bool aoriented=true) pure nothrow @safe @nogc { mName = aname; mOriented = aoriented; }

final:
  @property const pure nothrow @safe @nogc {
    bool valid () { pragma(inline, true); return (mNormalized || points.length >= 4); } ///
    bool normalized () { pragma(inline, true); return mNormalized; } ///
    bool oriented () { pragma(inline, true); return mOriented; } ///
    string name () { pragma(inline, true); return mName; } ///
    bool hasOriginalPoints () { pragma(inline, true); return (points.length != 0); } ///
    uint length () { pragma(inline, true); return cast(uint)points.length/2U; } /// number of original points
    alias opDollar = length;
    /// return original point
    Point opIndex (in uint idx) { pragma(inline, true); return (idx < cast(uint)points.length/2U ? Point(points[idx*2U+0U], points[idx*2U+1U]) : Point.init); }
    /// number of points in normalized (not rotated) pattern
    uint patPointCount () { pragma(inline, true); return patlength/2U; }
  }

  /// return normalized (not rotated) point, null if not normalized
  Point patPoint (in uint idx) const nothrow @trusted @nogc {
    pragma(inline, true);
    return (mNormalized && idx < patlength/2U ? Point(patpointsNorm.ptr[idx*2U+0U], patpointsNorm.ptr[idx*2U+1U]) : Point());
  }

  /// can't be changed for normalized glyphs with original points dropped
  @property void oriented (bool v) pure nothrow @safe @nogc {
    if (mNormalized && points.length < 4) return;
    if (mOriented != v) {
      mOriented = v;
      mNormalized = false;
    }
  }

  /// set new name
  @property void name(T:const(char)[]) (T v) nothrow @safe {
    static if (is(T == typeof(null))) mName = null;
    else static if (is(T == string)) mName = v;
    else { if (mName != v) mName = v.idup; }
  }

  /// will not clear the orientation
  auto clear () nothrow @trusted {
    delete points;
    mNormalized = false;
    mName = null;
    return this;
  }

  /// clone glyph
  auto clone () const @trusted {
    auto res = new PTGlyph();
    res.mName = mName;
    res.mNormalized = mNormalized;
    res.mOriented = mOriented;
    res.patpointsNorm[] = patpointsNorm[];
    res.patpoints[] = patpoints[];
    res.patlength = patlength;
    res.points = unsafeArrayDup(points);
    return res;
  }

  /// append point to glyph (mouse position, for example)
  // may turn the glyph into non-normalized
  auto appendPoint (int x, int y) nothrow @trusted {
    if (points.length > 0xffffffU) return this; // just in case
    float fx = cast(float)x;
    float fy = cast(float)y;
    if (points.length) {
      assert(points.length%2U == 0);
      // check distance and don't add points that are too close to each other
      immutable float lx = fx-points[$-2];
      immutable float ly = fy-points[$-1];
      if (lx*lx+ly*ly < MinPointDistance*MinPointDistance) return this;
    }
    unsafeArrayAppend(points, fx);
    unsafeArrayAppend(points, fy);
    mNormalized = false;
    return this;
  }

  /// create normalized patterns
  auto normalize(uint normpatlen=NormalizedPoints*2U) (bool dropOriginalPoints=true)
  if (normpatlen >= 2 && normpatlen <= MaxNormalizedPoints*2U && normpatlen%2U == 0)
  {
    if (!mNormalized) {
      if (points.length < 4) return this; //throw new Exception("glyph must have at least two points");
      patlength = normpatlen;
      resample!false(patpointsNorm[0..normpatlen], points);
      buildNormPoints(patpoints[0..normpatlen], points, mOriented);
      mNormalized = true;
    }
    if (dropOriginalPoints) { assert(mNormalized); delete points; }
    return this;
  }

  /// is score returned from `match()` means "recognized"?
  static bool isGoodScore (in float score) nothrow @safe @nogc {
    pragma(inline, true);
    import iv.nanpay : isFiniteF;
    version(use_gesture_doperchuk) {
      return (isFiniteF(score) ? score >= 0.88f : false); // arbitrary number
    } else {
      return (isFiniteF(score) ? score >= MinMatchScore : false);
    }
  }

  /// this: template; you can use `isGoodScore()` to see if it is a good score to detect a match
  float match (const(PTGlyph) sample) const nothrow @trusted @nogc {
    if (sample is null || !sample.valid || !valid) return -float.infinity;
    PatArray me = patpoints[];
    PatArray it = sample.patpoints[];
    uint patlen = patlength;
    if (!mNormalized) {
      // `this` is not normalized
      if (!sample.mNormalized) {
        // both `this` and `sample` are not normalized
        patlen = NormalizedPoints*2U;
        //{ import core.stdc.stdio : printf; printf("  resampling both to %u samples\n", patlen); }
        buildNormPoints(me[0..patlen], points, mOriented);
        buildNormPoints(it[0..patlen], sample.points, sample.mOriented);
      } else {
        // only `this`
        patlen = sample.patlength;
        //{ import core.stdc.stdio : printf; printf("  resampling `this` to %u samples\n", patlen); }
        buildNormPoints(me[0..patlen], points, mOriented);
      }
    } else if (!sample.mNormalized) {
      // `sample` is not normalized
      //{ import core.stdc.stdio : printf; printf("  resampling `sample` to %u samples (%u)\n", patlen, cast(uint)sample.points.length); }
      buildNormPoints(it[0..patlen], sample.points, sample.mOriented);
    } else if (patlen != sample.patlength) {
      // different number of pattern points, renormalize
      if (patlen < sample.patlength) {
        // renormalize `sample`
        if (sample.points.length >= 4) {
          //{ import core.stdc.stdio : printf; printf("  renormalizing `sample` to %u samples (from original points)\n", patlen); }
          buildNormPoints(it[0..patlen], sample.points, sample.mOriented);
        } else {
          //{ import core.stdc.stdio : printf; printf("  renormalizing `sample` to %u samples (from pattern points)\n", patlen); }
          buildNormPoints(it[0..patlen], sample.patpoints[0..sample.patlength], sample.mOriented);
        }
      } else {
        // renormalize `this`
        patlen = sample.patlength;
        if (points.length >= 4) {
          //{ import core.stdc.stdio : printf; printf("  renormalizing `this` to %u samples (from original points)\n", patlen); }
          buildNormPoints(me[0..patlen], points, mOriented);
        } else {
          //{ import core.stdc.stdio : printf; printf("  renormalizing `this` to %u samples (from pattern points)\n", patlen); }
          buildNormPoints(me[0..patlen], patpoints[0..patlength], mOriented);
        }
      }
    }
    return match(me[0..patlen], it[0..patlen], mOriented/*||sample.mOriented*/);
  }

  // [xmin, ymin, xmax, ymax]
  void calcPatPointBBox (float[] bbox) nothrow @trusted @nogc {
    calcBBox(bbox, patpointsNorm[0..patlength]);
  }

public:
  // [xmin, ymin, xmax, ymax]
  static void calcBBox (float[] bbox, const(float)[] pts) nothrow @trusted @nogc {
    if (pts.length%2) pts = pts[0..$-1]; // just in case
    if (pts.length < 2) return;
    if (bbox.length == 0) return;
    bbox.ptr[0] = +float.infinity;
    if (bbox.length > 1) bbox.ptr[1] = +float.infinity;
    if (bbox.length > 2) bbox.ptr[2] = -float.infinity;
    if (bbox.length > 3) bbox.ptr[3] = -float.infinity;
    uint bidx = 0;
    foreach (const float v; pts) {
      if (bbox.ptr[bidx] > v) bbox.ptr[bidx] = v;
      if (bbox.length > bidx+2U && bbox.ptr[bidx+2U] < v) bbox.ptr[bidx+2U] = v;
      bidx ^= 1;
    }
  }

private:
  static float distance (in float x0, in float y0, in float x1, in float y1) nothrow @safe @nogc {
    pragma(inline, true);
    import core.stdc.math : sqrtf;
    immutable float dx = x1-x0;
    immutable float dy = y1-y0;
    return sqrtf(dx*dx+dy*dy);
  }

  static float match (const(float)[] tpl, const(float)[] v1, in bool oriented) nothrow @trusted @nogc {
    pragma(inline, true);
    //{ import core.stdc.stdio : printf; printf("  STMATCH: tpl.length=%u; v1.length=%u\n", cast(uint)tpl.length, cast(uint)v1.length); }
    if (tpl.length != v1.length) return -float.infinity;
    version(use_gesture_doperchuk) {
      version(none) { import core.stdc.stdio : printf; printf("  STMATCH: tpl.length=%u; v1.length=%u\n", cast(uint)tpl.length, cast(uint)v1.length); }
      float sum = 0.0f;
      foreach (immutable idx; (oriented ? 0 : 1)..tpl.length/2U) {
        sum += tpl.ptr[idx*2U+0U]*v1.ptr[idx*2U+0U]+tpl.ptr[idx*2U+1U]*v1.ptr[idx*2U+1U];
        version(none) {
          import core.stdc.stdio : printf; printf("    tpl:(%f,%f); v1:(%f,%f); sum:%f\n",
          cast(double)tpl.ptr[idx*2U+0U], cast(double)tpl.ptr[idx*2U+1U],
          cast(double)v1.ptr[idx*2U+0U], cast(double)v1.ptr[idx*2U+1U],
          cast(double)sum);
        }
      }
      sum /= cast(float)cast(int)(tpl.length/2U-(oriented ? 0U : 1U));
      version(none) { import core.stdc.stdio : printf; printf("   RES=%f\n", cast(double)sum); }
      return sum;
    } else {
      return 1.0f/optimalCosineDistance(tpl, v1);
    }
  }

  version(use_gesture_doperchuk) {} else
  static float optimalCosineDistance (const(float)[] v0, const(float)[] v1) nothrow @trusted @nogc {
    import core.stdc.math : atanf, acosf, cosf, sinf, acos, cos, sin;
    if (v0.length != v1.length || v0.length%2U != 0) return -float.infinity;
    float a = 0.0f, b = 0.0f;
    foreach (immutable idx; 0..v0.length/2U) {
      //{ import core.stdc.stdio : printf; printf("    CDS: idx=%u; v0=(%f,%f); v1=(%f,%f)\n", cast(uint)idx, v0.ptr[idx*2U+0U], v0.ptr[idx*2U+1U], v1.ptr[idx*2U+0U], v1.ptr[idx*2U+1U]); }
      a += v0.ptr[idx*2U+0U]*v1.ptr[idx*2U+0U]+v0.ptr[idx*2U+1U]*v1.ptr[idx*2U+1U];
      b += v0.ptr[idx*2U+0U]*v1.ptr[idx*2U+1U]-v0.ptr[idx*2U+1U]*v1.ptr[idx*2U+0U];
    }
    immutable float angle = atanf(b/a);
    //{ import core.stdc.stdio : printf; printf("    CDS: a=%f; b=%f; angle=%f\n", cast(double)a, cast(double)b, cast(double)angle); }
    return acosf(a*cosf(angle)+b*sinf(angle));
  }

  // total path length
  static float pathLength (const(float)[] points) nothrow @trusted @nogc {
    float res = 0.0f;
    if (points.length >= 4) {
      float px = points.ptr[0U];
      float py = points.ptr[1U];
      foreach (immutable idx; 1..points.length/2U) {
        immutable float cx = points.ptr[idx*2U+0U];
        immutable float cy = points.ptr[idx*2U+1U];
        res += distance(px, py, cx, cy);
        px = cx;
        py = cy;
      }
    }
    return res;
  }

  static void resample(bool doper) (float[] dest, const(float)[] points) nothrow @trusted @nogc {
    assert(dest.length >= 4 && dest.length%2U == 0);
    assert(points.length >= 4 && points.length%2U == 0);
    immutable float I = pathLength(points)/cast(float)(cast(int)dest.length/2-1); // interval length
    //version(all) { import core.stdc.stdio : printf; printf("RESAMPLE: from %u to %u; ilen=%f; pathlen=%f\n", cast(uint)points.length, cast(uint)dest.length, cast(double)I, pathLength(points)); }
    float D = 0.0f;
    float prx = points.ptr[0];
    float pry = points.ptr[1];
    // add first point as-is
    dest.ptr[0] = prx;
    dest.ptr[1] = pry;
    usize ptpos = 2, oppos = 2;
    while (oppos < points.length && points.length-oppos >= 2) {
      immutable float cx = points.ptr[oppos], cy = points.ptr[oppos+1];
      immutable d = distance(prx, pry, cx, cy);
      //version(all) { import core.stdc.stdio : printf; printf("  spos=%u; dpos=%u; D+d=%f; I=%f; \n", cast(uint)oppos, cast(uint)ptpos, cast(double)(D+d), cast(double)I); }
      if (D+d >= I) {
        immutable float dd = (I-D)/d;
        immutable float qx = prx+dd*(cx-prx);
        immutable float qy = pry+dd*(cy-pry);
        assert(ptpos+1 < dest.length);
        dest.ptr[ptpos++] = qx;
        dest.ptr[ptpos++] = qy;
        // use 'q' as previous point
        prx = qx;
        pry = qy;
        D = 0.0f;
      } else {
        D += d;
        prx = cx;
        pry = cy;
        oppos += 2;
      }
    }
    // somtimes we fall a rounding-error short of adding the last point, so add it if so
    if (ptpos/2U == dest.length/2U-1U) {
      dest.ptr[ptpos++] = points[$-2];
      dest.ptr[ptpos++] = points[$-1];
    }
    assert(ptpos == dest.length);
    // for Doperchuk Improved, make each point relative to previous, and normalize it
    static if (doper) {
      import core.stdc.math : sqrtf;
      // hack for oriented gestures: use angle from the center to the first point to check orientation
      float[4] bbox = void;
      calcBBox(bbox[], dest);
      float px = dest.ptr[0]+(bbox[2]-bbox[0])*0.5f;
      float py = dest.ptr[1]+(bbox[3]-bbox[1])*0.5f;
      foreach (immutable idx; 0..ptpos/2U) {
        immutable float x = dest.ptr[idx*2U+0U];
        immutable float y = dest.ptr[idx*2U+1U];
        // offset
        immutable float xo = x-px;
        immutable float yo = y-py;
        // normalization
        immutable float mag = sqrtf(xo*xo+yo*yo);
        dest.ptr[idx*2U+0U] = xo/mag;
        dest.ptr[idx*2U+1U] = yo/mag;
        version(none) {
          import core.stdc.stdio : printf; printf("NORM %u: x=%f; y=%f; xo=%f; yo=%f; nx=%f; ny=%f; mag=%f; nlen=%f\n",
            cast(uint)idx,
            cast(double)x, cast(double)y,
            cast(double)xo, cast(double)yo,
            cast(double)(xo/mag), cast(double)(yo/mag),
            cast(double)mag,
            cast(double)(sqrtf((xo/mag)*(xo/mag)+(yo/mag)*(yo/mag)))
          );
        }
        px = x;
        py = y;
      }
    }
  }

  // stroke is not required to be centered, but it must be resampled
  version(use_gesture_doperchuk) {} else
  static void vectorize (float[] vres, const(float)[] ptx, bool orientationSensitive) nothrow @trusted @nogc {
    assert(vres.length == ptx.length);
    assert(vres.length >= 4 && vres.length%2 == 0);
    assert(vres.length <= MaxNormalizedPoints*2U);
    import core.stdc.math : atan2f, cosf, sinf, floorf, sqrtf;
    enum float PI_F = cast(float)0x1.921fb54442d18469898cc51701b84p+1L; /* PI = 3.141592... */
    PatArray pts = void;
    float cx = 0, cy = 0;
    // center it
    foreach (immutable idx; 0..vres.length/2U) {
      cx += ptx.ptr[idx*2+0];
      cy += ptx.ptr[idx*2+1];
    }
    immutable float dvr = cast(float)cast(int)(vres.length/2U);
    cx /= dvr;
    cy /= dvr;
    foreach (immutable idx; 0..vres.length/2U) {
      pts.ptr[idx*2+0] = ptx.ptr[idx*2+0]-cx;
      pts.ptr[idx*2+1] = ptx.ptr[idx*2+1]-cy;
    }
    // calc angle to rotate
    immutable float indAngle = atan2f(pts.ptr[1], pts.ptr[0]); // always must be done for centered stroke
    float delta = indAngle;
    if (orientationSensitive) {
      immutable baseOrientation = (PI_F/4.0f)*floorf((indAngle+PI_F/8.0f)/(PI_F/4.0f));
      delta = baseOrientation-indAngle;
    }
    // perform rotation, and calculate magnitude
    immutable float cosd = cosf(delta);
    immutable float sind = sinf(delta);
    float sum = 0.0f;
    foreach (immutable idx; 0..vres.length/2) {
      immutable nx = pts.ptr[idx*2+0]*cosd-pts.ptr[idx*2+1]*sind;
      immutable ny = pts.ptr[idx*2+1]*cosd+pts.ptr[idx*2+0]*sind;
      vres.ptr[idx*2+0] = nx;
      vres.ptr[idx*2+1] = ny;
      sum += nx*nx+ny*ny;
    }
    immutable float magnitude = sqrtf(sum);
    // normalize lengthes
    foreach (ref float v; vres[]) v /= magnitude;
  }

  static void buildNormPoints (float[] dest, const(float)[] points, bool orientationSensitive) nothrow @trusted @nogc {
    assert(dest.length >= 4 && dest.length%2U == 0 && dest.length <= MaxNormalizedPoints*2U);
    assert(points.length >= 4 && points.length%2U == 0);
    float[MaxNormalizedPoints*2U] tmp = void;
    resample!DoperchukImproved(tmp[0..dest.length], points);
    version(use_gesture_doperchuk) {
      // nothing more to do
      dest[] = tmp[0..dest.length];
    } else {
      vectorize(dest[], tmp[0..dest.length], orientationSensitive);
    }
  }

public:
  // find matching gesture for this one (i.e. use `this` as a sample)
  // you'd better properly normalize `this` before searching
  // outscore is NaN if match wasn't found
  const(PTGlyph) findMatch (const(PTGlyph)[] list, float* outscore=null) const nothrow @trusted @nogc {
    import iv.nanpay : isFiniteF;
    float bestScore = -float.infinity;
    PTGlyph res = null;
    if (outscore !is null) *outscore = float.nan;
    if (valid) {
      // build normalized `this` glyph in pts
      foreach (const PTGlyph gs; list) {
        if (gs is null || !gs.valid) continue;
        immutable float score = gs.match(this);
        //{ import core.stdc.stdio; printf("tested: '%.*s'; score=%f\n", cast(int)gs.mName.length, gs.mName.ptr, cast(double)score); }
        if (isGoodScore(score) && score > bestScore) {
          //{ import core.stdc.stdio; printf("  BETTER! '%.*s' (prevbest=%f)\n", cast(int)gs.mName.length, gs.mName.ptr, cast(double)bestScore); }
          bestScore = score;
          res = cast(PTGlyph)gs; // sorry
        }
      }
    }
    if (res !is null && outscore !is null) *outscore = bestScore;
    return res;
  }

public:
  static void wrVarInt (VFile fl, uint n) {
    do {
      ubyte v = cast(ubyte)(n&0x7fU);
      n >>= 7;
      if (n) v |= 0x80U;
      fl.writeNum!ubyte(v);
    } while (n != 0);
  }

  static uint rdVarInt (VFile fl) {
    uint n = 0;
    ubyte shift = 0;
    uint v;
    do {
      v = fl.readNum!ubyte;
      if (shift >= 28) {
        if (v > 0x0fU) throw new Exception("invalid varint");
      }
      n |= (v&0x7fU)<<shift;
      shift += 7;
    } while (v&0x80U);
    return n;
  }

  void saveV3 (VFile fl) const {
    string gname = mName;
    if (gname.length > 1023) gname = gname[0..1023];
    wrVarInt(fl, cast(uint)gname.length);
    fl.rawWriteExact(gname);
    // "oriented" flag
    fl.writeNum!ubyte(mOriented ? 1 : 0);
    if (mNormalized) {
      // has normalized points
      wrVarInt(fl, cast(uint)patlength/2U);
      foreach (immutable float pt; patpointsNorm[0..patlength]) fl.writeNum!float(pt);
      version(use_gesture_doperchuk) {
        fl.writeNum!ubyte(cast(ubyte)0); // "no rotated points" flag
      } else {
        fl.writeNum!ubyte(cast(ubyte)1); // "has rotated points" flag
        foreach (immutable float pt; patpoints[0..patlength]) fl.writeNum!float(pt);
      }
    } else {
      // no normalized points
      PatArray ptmpsm = void;
      version(use_gesture_doperchuk) {
        // Doperchuk Improved
        enum pcount = NormalizedPoints;
      } else {
        // protractor
        enum pcount = MaxNormalizedPoints;
      }
      resample!false(ptmpsm[0..pcount*2U], points);
      wrVarInt(fl, cast(uint)pcount);
      foreach (immutable pt; ptmpsm[0..pcount*2U]) fl.writeNum!float(pt);
      fl.writeNum!ubyte(cast(ubyte)0); // "no rotated points" flag
    }
  }

  static usize rdXNum (VFile fl) {
    ubyte v = fl.readNum!ubyte;
    if (v < 254) return cast(usize)v;
    if (v == 254) {
      ulong nv = fl.readNum!ulong;
      if (nv > usize.max) throw new Exception("number too big");
      return cast(usize)nv;
    } else {
      assert(v == 255);
      return cast(usize)fl.readNum!uint;
    }
  }

  static PTGlyph loadNew (VFile fl, int ver) {
    float rdFloat () {
      float fv = fl.readNum!float;
      if (fv != fv) throw new Exception("invalid floating number"); // nan check
      return fv;
    }

    static void renormalize (float[] pts) {
      if (pts.length < 4) return;
      float[4] bbox = void;
      calcBBox(bbox[], pts);
      immutable float wdt = bbox[2]-bbox[0];
      immutable float hgt = bbox[3]-bbox[1];
      if (wdt >= 16.0f || hgt >= 16.0f) return;
      foreach (ref float fv; pts[]) fv = fv*64.0f+32.0f;
    }

    if (ver == 0 || ver == 1) {
      // name
      auto len = fl.readNum!uint();
      if (len > 1024) throw new Exception("glyph name too long");
      auto res = new PTGlyph();
      scope(failure) { res.clear(); delete res; }
      if (len > 0) {
        auto buf = new char[](len);
        fl.rawReadExact(buf);
        res.mName = cast(string)buf; // it is safe to cast here
      }
      // template
      static if (NormalizedPoints != 16) throw new Exception("cannot load unknown number of pattern points");
      float[16*2] tmp = void;
      res.patlength = 16U*2U;
      foreach (ref pt; tmp[]) pt = rdFloat();
      renormalize(tmp[]);
      res.patpointsNorm[0..res.patlength] = tmp[];
      res.mNormalized = true;
      res.mOriented = true;
      if (ver == 1) res.mOriented = (fl.readNum!ubyte != 0);
      buildNormPoints(res.patpoints[0..res.patlength], tmp[], res.mOriented);
      return res;
    } else if (ver == 2) {
      // name
      auto nlen = rdXNum(fl);
      if (nlen > int.max/4) throw new Exception("glyph name too long");
      auto res = new PTGlyph();
      if (nlen) {
        auto nbuf = new char[](nlen);
        fl.rawReadExact(nbuf);
        res.mName = cast(string)nbuf; // it is safe to cast here
      }
      // "oriented" flag
      res.mOriented = (fl.readNum!ubyte != 0);
      res.patlength = NormalizedPoints*2U;
      // normalized points
      auto nplen = rdXNum(fl);
      if (nplen != 0) {
        if (nplen < 3 || nplen > ushort.max) throw new Exception("invalid number of resampled points");
        if (nplen != NormalizedPoints) {
          float[] opts;
          scope(exit) delete opts;
          opts.reserve(nplen*2);
          normBlkAttr(opts);
          foreach (immutable pidx; 0..nplen*2) opts ~= rdFloat();
          buildNormPoints(res.patpoints[0..res.patlength], opts[], res.mOriented);
          resample!false(res.patpointsNorm, opts[]);
          res.mNormalized = true;
        } else {
          // direct loading
          version(use_gesture_doperchuk) {
            // read and unnormalize
            foreach (ref float fv; res.patpointsNorm[0..res.patlength]) fv = rdFloat();
            renormalize(res.patpointsNorm[0..res.patlength]);
            buildNormPoints(res.patpoints[0..res.patlength], res.patpointsNorm[0..res.patlength], res.mOriented);
          } else {
            foreach (ref float fv; res.patpoints[0..res.patlength]) fv = rdFloat();
            res.patpointsNorm[] = res.patpoints[];
            renormalize(res.patpointsNorm[0..res.patlength]);
          }
        }
        res.mNormalized = true;
      }
      // original points
      auto plen = rdXNum(fl);
      if (plen) {
        if (plen%2 != 0) throw new Exception("invalid number of points");
        res.points.reserve(plen);
        normBlkAttr(res.points);
        foreach (immutable c; 0..plen) res.points ~= rdFloat();
      }
      if (plen >= 4) res.normalize(dropOriginalPoints:false);
      return res;
    } else if (ver == 3) {
      // name
      uint nlen = rdVarInt(fl);
      if (nlen > 1023) throw new Exception("invalid name length");
      auto res = new PTGlyph();
      scope(failure) { res.clear(); delete res; }
      if (nlen > 0) {
        auto buf = new char[](nlen);
        fl.rawReadExact(buf);
        res.mName = cast(string)buf; // it is safe to cast here
      }
      //{ import iv.vfs.io; writeln("loading [", res.mName, "]..."); }
      // "oriented" flag
      res.mOriented = (fl.readNum!ubyte != 0);
      uint ptcount = rdVarInt(fl);
      //{ import iv.vfs.io; writeln(" pcount=", ptcount, "; oriented=", res.mOriented); }
      if (ptcount < 2 || ptcount > MaxNormalizedPoints) throw new Exception("invalid number of normalized points");
      res.patlength = ptcount*2U;
      foreach (ref pt; res.patpointsNorm[0..res.patlength]) pt = rdFloat();
      //renormalize(res.patpointsNorm[0..res.patlength]);
      //version(all) { import iv.vfs.io; foreach (immutable idx; 0..ptcount) writeln("  ", idx, "; x=", res.patpointsNorm[idx*2+0], "; y=", res.patpointsNorm[idx*2+1]); }
      // has rotated points?
      if (fl.readNum!ubyte) {
        foreach (ref pt; res.patpoints[0..res.patlength]) pt = rdFloat();
      } else {
        // create rotated points
        version(use_gesture_doperchuk) {
          // for Doperchuk Improved, see below
        } else {
          vectorize(res.patpoints[0..res.patlength], res.patpointsNorm[0..res.patlength], res.mOriented);
        }
      }
      version(use_gesture_doperchuk) {
        // Doperchuk Improved, always recreate pattern points
        resample!DoperchukImproved(res.patpoints[0..res.patlength], res.patpointsNorm[0..res.patlength]);
      }
      res.mNormalized = true;
      return res;
    } else if (ver == 666) {
      // $P
      ubyte len = fl.readNum!ubyte;
      char[] nn;
      if (len == 255) {
        ushort xlen = fl.readNum!ushort;
        nn.length = xlen;
      } else {
        nn.length = len;
      }
      fl.rawReadExact(nn);
      version(none) {
        import iv.vfs.io;
        writeln("*** [", nn, "] ***");
      }
      auto res = new PTGlyph();
      res.mName = cast(string)nn; // it is safe to cast here
      scope(failure) { res.clear(); delete res; }
      uint ptcount = fl.readNum!ubyte;
      if (ptcount < 16 || ptcount > MaxNormalizedPoints) throw new Exception("invalid number of points in cloud");
      res.mOriented = true;
      res.patlength = ptcount*2U;
      foreach (immutable idx; 0..ptcount) {
        float x = cast(float)fl.readNum!float;
        float y = cast(float)fl.readNum!float;
        /*uint id =*/ fl.readNum!uint; // ignore ids, my old code is buggy with them
        res.patpointsNorm[idx*2U+0U] = x;
        res.patpointsNorm[idx*2U+1U] = y;
      }
      renormalize(res.patpointsNorm[0..res.patlength]);
      // create rotated points
      version(use_gesture_doperchuk) {
        // Doperchuk Improved
        resample!DoperchukImproved(res.patpoints[0..res.patlength], res.patpointsNorm[0..res.patlength]);
      } else {
        // protractor
        vectorize(res.patpoints[0..res.patlength], res.patpointsNorm[0..res.patlength], res.mOriented);
      }
      res.mNormalized = true;
      //res.normalize!(32U*2U)(); // use all 32 points, why not
      return res;
    } else {
      assert(0, "wtf?!");
    }
  }
}


// ////////////////////////////////////////////////////////////////////////// //
public void gstLibLoadEx (VFile fl, scope void delegate (PTGlyph) appendGlyph) {
  PTGlyph[] res;
  char[8] sign;
  fl.rawReadExact(sign[]);
  if (sign[0..$-1] == "K8PTRDB") {
    ubyte ver = cast(ubyte)sign[$-1];
    if (ver < '0' || ver > '9') throw new Exception("invalid gesture library signature");
    ver -= '0';
    if (ver > 3) throw new Exception("invalid gesture library version");
    if (ver == 0 || ver == 1) {
      // versions 0 and 1
      uint count = fl.readNum!uint;
      if (count > uint.max/8) throw new Exception("too many glyphs");
      foreach (immutable c; 0..count) {
        auto g = PTGlyph.loadNew(fl, ver);
        if (appendGlyph !is null) appendGlyph(g);
      }
    } else if (ver == 2) {
      // version 2
      while (fl.tell < fl.size) {
        auto g = PTGlyph.loadNew(fl, ver);
        if (appendGlyph !is null) appendGlyph(g);
      }
    } else {
      // version 3
      uint count = PTGlyph.rdVarInt(fl);
      if (count > 0xffffff) throw new Exception("too many gesture templates");
      while (count--) {
        auto g = PTGlyph.loadNew(fl, ver);
        if (appendGlyph !is null) appendGlyph(g);
      }
    }
  } else if (sign[] == "DOLP8LB0") {
    uint count = fl.readNum!uint;
    if (count > uint.max/16) throw new Exception("too many gestures in library");
    foreach (immutable idx; 0..count) {
      auto g = PTGlyph.loadNew(fl, 666);
      if (appendGlyph !is null) appendGlyph(g);
    }
  } else {
    throw new Exception("invalid gesture library signature");
  }
}


public PTGlyph[] gstLibLoad (VFile fl) {
  PTGlyph[] res;
  fl.gstLibLoadEx(delegate (PTGlyph g) { res ~= g; });
  return res;
}


// ////////////////////////////////////////////////////////////////////////// //
public void gstLibSave (VFile fl, const(PTGlyph)[] list) {
  if (list.length > 0xffffff) throw new Exception("too many glyphs");
  fl.rawWriteExact("K8PTRDB3");
  PTGlyph.wrVarInt(fl, cast(uint)list.length);
  foreach (const PTGlyph g; list) g.saveV3(fl);
}
